package application;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

public class State {
	
	private DataBase db;
	private Table t;
	private SavedDataBase s;
	private SaveTable st;
	private org.w3c.dom.Document doc;

	public org.w3c.dom.Document getDoc() {
		return doc;
	}

	public void setDoc() throws SAXException, IOException {
	    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = null;
        try {
            docBuilder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        File fXmlFile = new File("./src/file.xml");
        doc = docBuilder.parse(fXmlFile);
        doc.getDocumentElement().normalize();
	}

	public SaveTable getSt() {
		return st;
	}

	public void setSt(SaveTable st) {
		this.st = st;
	}

	public SavedDataBase getS() {
		return s;
	}

	public void setS(SavedDataBase s) {
		this.s = s;
	}

	public DataBase getDb() {
		return db;
	}

	public void setDb(DataBase db) {
		this.db = db;
	}

	public Table getT() {
		return t;
	}

	public void setT(Table t) {
		this.t = t;
	}
	
	
}
