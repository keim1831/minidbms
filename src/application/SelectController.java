package application;
import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class SelectController implements IMainViewController {
	private static final int PORT = 4556;
    BufferedReader input = new BufferedReader(
            new InputStreamReader(System.in));
    
	@FXML
	private TextField column;
	@FXML private ChoiceBox<String> agrFunction;
	@FXML private TextField agrFuncField;
	@FXML private TextField groupBy;
	@FXML private TextField having;
	@FXML private TextField where;
	@FXML  Label usedDb;
	@FXML  Label usedTable;
	@FXML private Label from;
	@FXML TextField join;
	@FXML TextArea result;
	@FXML TextField selectOutput;
	@FXML ScrollBar sbMain;

	
	public Router router;
	State state;
	
	public void currentT() {
		String str = state.getT().getCurrentTable();
		usedTable.setText(str);
		from.setText(str);
	}

	public void currentDB() {
		String str = state.getDb().getCurrentDB();
		usedDb.setText(str);
	}

	
	@Override
	public void load(Router router, State state) {
		this.router = router;
		this.state = state;
		currentDB();
		currentT();
	}
	
	@FXML
	private void back(ActionEvent event) throws IOException {
		router.toOption();
	}
	
	private ArrayList<String> getList() throws Exception{
		ArrayList<String> optionList = new ArrayList<String>();
		optionList.add("count");
		optionList.add("min");
		optionList.add("max");
		optionList.add("avg");
		optionList.add("sum");
		
		return optionList;
	}
	
	@FXML
	private void initialize() throws Exception {
		 sbMain.setMin(0);
	     sbMain.setMax(selectOutput.getText().length());
	     


	     sbMain.valueProperty().addListener((obs, oldVal, newVal)->{
	            System.out.println(newVal);
			 selectOutput.positionCaret(newVal.intValue());
	     });

		
		var choices = FXCollections.observableArrayList(getList());
		agrFunction.setItems(choices);
		agrFunction.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
	      @Override
	      public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
	        try {
			} catch (Exception e) {
			}
	      }
	    });
	}

	@FXML
	private void toTables(ActionEvent event) throws IOException{
		router.toTable();
	}
	
	@FXML
	private void toDatabase(ActionEvent event) throws IOException{
		router.toDatabase();
	}
	
	
	@FXML
	private void next(ActionEvent event) throws IOException{
		router.toFinish();
	
	}
	@FXML 
	private void runQuery(ActionEvent event) throws Exception{
		try(Socket socket= new Socket("127.0.0.1",PORT)){
	        DataOutputStream output = new DataOutputStream(socket.getOutputStream());
	     	//String str = "select "+column.getText()+" FROM " +usedTable.getText()+ " JOIN "+ join.getText()+" WHERE "+where.getText();
	        //System.out.println(str);
	        //output.writeUTF(str);
            //output.writeUTF("select students.student_id students.name groups.department FROM students JOIN groups ON students.group_id = groups._id WHERE students.age = 20");
			//output.writeUTF("select students.name avg(students.age) FROM students JOIN groups ON students.group_id = groups.group_id WHERE students.group_id = 0 GROUP BY students.name HAVING avg(students.age) > 20 AND avg(students.age) < 28");
			//output.writeUTF("select students.student_id max(grades.grade) FROM grades JOIN teachers ON grades.teacher_id = teachers.teacher_id JOIN students ON grades.student_id = students.student_id WHERE students.age > 20 GROUP BY students.student_id");
	        String str = "select "+column.getText();
	        if(!agrFuncField.getText().isEmpty())
	        	str = str.concat(" "+agrFunction.getValue()+"("+agrFuncField.getText()+ ")");
	        
	        str = str.concat(" FROM " +usedTable.getText());
	        if(!join.getText().isEmpty()) {
	        	str = str.concat(" JOIN "+ join.getText());
	        	System.out.println(str);
	        }
	        if(!where.getText().isEmpty())
	        	str = str.concat(" WHERE "+where.getText());
	        if(!groupBy.getText().isEmpty())
	        	str = str.concat(" GROUP BY " + groupBy.getText());
	        if(!having.getText().isEmpty())
	        	str = str.concat(" HAVING " +having.getText());
	        System.out.println(str);
            //output.writeUTF("select students.student_id students.name subjects.name max(grades.grade) FROM students JOIN grades ON grades.student_id = students.student_id JOIN subjects ON subjects.subject_id = grades.subject_id WHERE students.age > 20 GROUP BY students.student_id students.name subjects.name HAVING count(students.student_id) > 10");
	        output.writeUTF(str);
	        //output.writeUTF("select students.name FROM students WHERE students.group_id = 0 GROUP BY students.name HAVING avg(students.age) > 20");
	        //output.writeUTF("select students.name avg(students.age) FROM students JOIN groups ON students.group_id = groups.group_id WHERE students.group_id = 0 GROUP BY students.name");

	        
	        DataInputStream dataInput = new DataInputStream(socket.getInputStream());
			String answer = dataInput.readUTF();
			BufferedReader in = new BufferedReader(new FileReader("temp.txt"));
			StringBuilder selectResult = new StringBuilder();
			String line = in.readLine();
			selectResult.append(line);
			while (line!= null){
				 line = in.readLine();
				 if(line!=null) {
					 System.out.println(line);
					 selectResult.append(line);
				 }

			}
			System.out.println("Answer: "+answer);
			selectOutput.setText(selectResult.toString());
		    System.out.println("changed");
		    System.out.println(selectOutput.getText());
			in.close();
	        
	        socket.close();
	    }catch (UnknownHostException e) {
	        System.out.println("ERROR! Server not found!");
	    }
	    catch (IOException e2) {

	    }
		router.toSelect();
	}
	
}
