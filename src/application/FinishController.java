package application;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;

public class FinishController implements IMainViewController {
	
	public Router router;
	@Override
	public void load(Router router, State state) {
		this.router = router;
	}
	
	
	@FXML
	private void toFirst(ActionEvent event) throws IOException{
		router.toWelcome();
	}
	
	@FXML
	private void exitButton(ActionEvent event) throws IOException{
		router.exit();
	}



}
