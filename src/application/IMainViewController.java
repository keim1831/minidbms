package application;

public interface IMainViewController {
	void load(Router router, State state);
}
