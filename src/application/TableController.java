package application;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.mongodb.MongoClient;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;


public class TableController implements IMainViewController {
	private Router router;
	String selectedTable;
	State state;
	private Document doc;
   
	
	
	public TableController() throws Exception {
		try {
		    File file = new File("./src/file.xml");
		    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		    DocumentBuilder db  = dbf.newDocumentBuilder();
		    doc = db.parse(file);  
			doc.getDocumentElement().normalize();
		} catch (ParserConfigurationException e) {
		    e.printStackTrace();
		}
	}

	private static final int PORT = 4556;
    BufferedReader input = new BufferedReader(
            new InputStreamReader(System.in));
	
	@FXML TableView<Arguments> tableStructure;
	@FXML TableColumn<Arguments, String> nameColumn;
	@FXML TableColumn<Arguments, String> typeColumn;
	@FXML TableColumn<Arguments, String> sizeColumn;
	@FXML TableColumn<Arguments, String> keyTypeColumn;
	@FXML TextField addName;
	@FXML TextField addType;
	@FXML TextField addSize;
	@FXML TextField addKeyType;
	@FXML Button add;
	@FXML Button nextButton;
	@FXML private CheckBox checkBox;

	private final ObservableList<Arguments> data = FXCollections.observableArrayList();
	
	@FXML Label label;
	@FXML ChoiceBox<String> tableChoiceBox;
	@FXML Label selected;
	@FXML TextField newTable;
	@FXML
	
	private void ckeckIfChecked() {

                 if (selected.getText().length() > 0) {
             		nextButton.setVisible(true);

                	 nextButton.setDisable(false);

		        	//settingCurrendDB(selected.getText());

                	 //s.setDatabase(selected.getText());

                 }

	}
	
	
	public void currentDB() {
		String str = state.getDb().getCurrentDB();
		label.setText(str);
	}
	
	public void settingCurrentTable(String str) {
		Table t = new Table();
		t.setCurrentTable(str);
		state.setT(t);
	}
	public void saveT(String str) {
		SaveTable s = new SaveTable();
		s.setSaveTable(str);
		state.setSt(s);
	}

	@Override
	public void load(Router router, State state){
		this.router = router;
		this.state = state;
		
		currentDB();
	}
	
	@FXML
	private void back(ActionEvent event) throws IOException {
		router.toDatabase();
	}
	
	@FXML
	private void next(ActionEvent event) throws IOException{
		router.toOption();
	}
	
	@FXML
	private void saveNewTable(ActionEvent event) throws IOException{
			
		String text = newTable.getText();
		
		if(text != null) {
			saveT(text);
			 try(Socket socket= new Socket("127.0.0.1",PORT)){
		            DataOutputStream output = new DataOutputStream(socket.getOutputStream());
		            
		            if(state.getSt() != null) {
						output.writeUTF("create table exams");
						output.writeUTF("4 exam_id 0 int 0 student_id 0 int 0 grade 0 int 0 subject_id 0 int 0");
//		        		output.writeUTF("create table " + newTable.getText());
//						output.writeUTF(tableStructure.);
		        		//output.writeUTF("100 " + state.getSt().getSaveTable() +".kv" + state.getSt().getSaveTable() + tableStructure.getItems().size());//;
		        		socket.close();
				    	router.toTable();
		            }
		           
		        }catch (UnknownHostException e) {
		            System.out.println("ERROR! Server not found!");
				 router.toTable();
		        }
		        catch (IOException e2) {
					router.toTable();
		        }
	    
		}
		
		router.toTable();
		
	}
	
	@FXML 
	private void choiceList() throws Exception {
		var choices = FXCollections.observableArrayList(getList());
		tableChoiceBox.setItems(choices);
		tableChoiceBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
		      @Override
		      public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
		        try {
		        	selected.setText(newValue);
		        	settingCurrentTable(newValue);
		        			        
				} catch (Exception e) {
				}
		      }
		    });
	}
	
	
	private ArrayList<String> getList() throws IOException{
		ArrayList<String> ar = new ArrayList<String>();
		try   
		{  
			File file = new File("src\\file.xml");  
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();  
			DocumentBuilder db = dbf.newDocumentBuilder();  
			Document doc = db.parse(file);  
			doc.getDocumentElement().normalize();
			
			NodeList nodeList = doc.getElementsByTagName("Table");  
		
			for (int itr = 0; itr < nodeList.getLength(); itr++)   
			{  
				
				Element e = (Element)nodeList.item(itr);
		        var description = e.getAttribute("tableName");
		        ar.add(description);
			
			}
		}     
		catch (Exception e)   
		{  
			e.printStackTrace();  
		}  
		
		    
		return ar;
	}

	
	@FXML
	protected void initialize() throws Exception {
		System.out.println("TableController");
		nextButton.setVisible(false);
		/*
		ArrayList<String> ar = new ArrayList<String>();
		String dbName = "University"; 
		//state.getDb().getCurrentDB();
		 //System.out.println(state.getDb().getCurrentDB());
		NodeList dbNodes = doc.getElementsByTagName("DataBase");
		Node dbNode;
        int x;
        for( x=0; x<dbNodes.getLength(); x++) {
            String name = dbNodes.item(x).getAttributes().getNamedItem("dataBaseName").getNodeValue();

            if(name.equals(dbName)){
                dbNode = dbNodes.item(x);
                NodeList table = dbNode.getChildNodes();
    		    //System.out.println(table.getLength());
    		    if(table!=null) {
    		        for (int i = 0; i < table.getLength(); i++) {
    		            //System.out.println(table.item(x).getAttributes());
    		            String namee = table.item(x).getAttributes().getNamedItem("tableName").getNodeValue();
    		            //System.out.println(name);
    		            ar.add(namee);
    		        }
    		    }


            }
        }
		
		*/
		var choices = FXCollections.observableArrayList(getList());
		tableChoiceBox.setItems(choices);
		tableChoiceBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
		      @Override
		      public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
		        try {
		        	selected.setText(newValue);
		        	settingCurrentTable(newValue);
		        			        
				} catch (Exception e) {
				}
		      }
		    });
		
		tableStructure.setEditable(true);
        tableStructure.setItems(data);
        //tableStructure.getColumns().addAll(nameColumn, typeColumn, sizeColumn, keyTypeColumn);


		nameColumn.setCellValueFactory(new PropertyValueFactory<Arguments, String>("name"));
		typeColumn.setCellValueFactory(new PropertyValueFactory<Arguments, String>("type"));
		sizeColumn.setCellValueFactory(new PropertyValueFactory<Arguments, String>("size"));
		keyTypeColumn.setCellValueFactory(new PropertyValueFactory<Arguments, String>("keyType"));
		
        add.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                data.add(new Arguments(
                        addName.getText(),
                        addType.getText(),
                        addSize.getText(),
                		addKeyType.getText()));
                addName.clear();
                addType.clear();
                addSize.clear();
                addKeyType.clear();
            }
        });
		
	}


	
}

