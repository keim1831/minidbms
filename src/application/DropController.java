package application;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Optional;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;

public class DropController implements IMainViewController{
	
	private static final int PORT = 4556;
    BufferedReader input = new BufferedReader(
            new InputStreamReader(System.in));
    
  
	
	@FXML Label selected;
	@FXML Label db;
	public Router router;
	State state;
	

	
	public void currentT() {
		String str = state.getT().getCurrentTable();
		selected.setText(str);
	}
	
	public void currentDB() {
		String str = state.getDb().getCurrentDB();
		db.setText(str);
	}

	public void settingCurrentTable(String str) {
		Table t = new Table();
		t.setCurrentTable(str);
		state.setT(t);
	}

	
	@Override
	public void load(Router router, State state) {
		this.router = router;
		this.state = state;
		currentT();
		currentDB();
	}

	
	
	public void showAlert() {
	    Platform.runLater(new Runnable() {
	      public void run() {
	    		Alert alert = new Alert(Alert.AlertType.NONE);

	    	  	alert.setTitle("Are you SURE? ");
	    		alert.setContentText("Do you really want to Drop the currently selected Table? "
	    				+ " This prosess cannot be undone ! If YES, please  press the Drop button . "
	    				+ "Else just close this dialog panel, and just  go back and chose another.");
	    		
	    		ButtonType cancelButton = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
	    		alert.getButtonTypes().setAll(cancelButton);
	            Optional<ButtonType> result = alert.showAndWait();
	    		if(result.get() == cancelButton)
	    		{
	    		    alert.close();
	    		}

	      }
	    });
	  	}
	
	@FXML
	protected void initialize() throws Exception {
		showAlert();
	}
	
	
	@FXML
	private void back(ActionEvent event) throws IOException {
		router.toOption();
	}
	

	@FXML
	private void toTables(ActionEvent event) throws IOException{
		router.toTable();
	}
	
	@FXML
	private void toDatabase(ActionEvent event) throws IOException{
		router.toDatabase();
	}
	
	
	@FXML
	private void next(ActionEvent event) throws IOException{
		router.toFinish();
	}
	@FXML
	public void dropButton(ActionEvent event) throws Exception{
		  try(Socket socket= new Socket("127.0.0.1",PORT)){
		        DataOutputStream output = new DataOutputStream(socket.getOutputStream());
		        
		        output.writeUTF("drop table "+selected.getText()+" "+db.getText());
		        socket.close();
		    }catch (UnknownHostException e) {
		        System.out.println("ERROR! Server not found!");
		    }
		    catch (IOException e2) {

		    }
		  settingCurrentTable("");
		  router.toOption();
		
	}


	
}
