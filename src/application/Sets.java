package application;

import java.util.ArrayList;

class Sets<T>{
    private ArrayList<T> sets;
    private int length;

    public Sets(){
        sets = new ArrayList<>();
        length = 0;
    }

    public void add(T element){
        sets.add(element);
        length++;

    }

    public T get(int i){
        return sets.get(i);
    }

    public int length(){
        return length;
    }

    public Sets<T> common(Sets<T> set2){
        Sets<T> output = new Sets<>();
        System.out.println("COMMON ELEMENTS:");
        for(int i = 0; i<length; i++){
            if(set2.sets.contains(sets.get(i))){
                output.add(sets.get(i));
                System.out.println(sets.get(i));
            }
        }
        return output;
    }

    public void equal(Sets<T> newSet){

        int i;

        for(i = 0; i<Math.min(newSet.length,this.length); i++){
            sets.set(i,newSet.get(i));
        }
        while( i< newSet.length){
            sets.add(newSet.get(i));
            i++;
        }

        length=newSet.length;
    }

    public boolean contains(T value){
        return sets.contains(value);
    }

}
