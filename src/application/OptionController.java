package application;
import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class OptionController implements IMainViewController {

	State state;
	private Router router;
	@FXML Label tableLabel;
	@FXML Label dbLabel;
	
	public void currentT() {
		String str = state.getT().getCurrentTable();
		tableLabel.setText(str);
	}
	
	public void currentDB() {
		String str = state.getDb().getCurrentDB();
		dbLabel.setText(str);
	}
	
	@Override
	public void load(Router router, State state) {
		this.router = router;
		this.state = state;
		
		currentDB();
		currentT();
	}
	
	@FXML
	private void toInsert(ActionEvent event) throws IOException {
		router.toInsert();
	}
	

	@FXML
	private void toUpdate(ActionEvent event) throws IOException{
		router.toSelect();
	}
	
	@FXML
	private void toDrop(ActionEvent event) throws IOException{
		router.toDrop();
	}
	
	@FXML
	private void toDropDB(ActionEvent event) throws IOException{
		router.toDropDB();
	}
	
	@FXML
	private void toIndex(ActionEvent event) throws IOException{
		router.toCreateIndex();
	}
	
	
	@FXML
	private void back(ActionEvent event) throws IOException{
		router.toTable();
	}

	
	
	
}
