package application;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.MongoWriteException;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.IndexOptions;
import com.mongodb.client.model.Indexes;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.util.*;

import static com.mongodb.client.model.Filters.*;


public class WriteXMLFile  {

    private org.w3c.dom.Document doc;
    private Element rootElement;
    private DOMSource source;
    private StreamResult result;
    private Transformer transformer;
    private Element db;
    private Element table;
    private Element structure;
    private MongoClient mongoClient;
    private String currentDBName;
    private MongoDatabase mongoDatabase;
    private ReadXMLFile readXMLFile;
    private HashMap<String, ArrayList<Integer>> finalIDs;
    private HashMap<String, ArrayList<Double>> aggregationValues;
    public PrintWriter pw;

    //useDB
    public void setCurrentDBName(String currentDBName) {
        this.mongoDatabase = mongoClient.getDatabase(currentDBName);
        this.currentDBName = currentDBName;
        db = (Element) readXMLFile.getDataBase(currentDBName);
        System.out.println(db);
    }

    public WriteXMLFile() throws IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = null;
        try {
            docBuilder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        File fXmlFile = new File("./src/file.xml");
        assert docBuilder != null;
        doc = docBuilder.parse(fXmlFile);//docBuilder.newDocument();
        rootElement =  doc.getDocumentElement();
//        doc.appendChild(rootElement);

        doc.getDocumentElement().normalize();
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        try {
            transformer = transformerFactory.newTransformer();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        }
        source = new DOMSource(doc);
        result = new StreamResult(fXmlFile);
        finalIDs = new HashMap<>();
        aggregationValues = new HashMap<>();

        //System.out.println("DOne");

    }

    public void connectMongoDB(){
        String uri = "mongodb+srv://m001-student:m001-mongodb-basics@anett-m00-jsqsm.mongodb.net/test?retryWrites=true&w=majority";
        MongoClientURI clientURI = new MongoClientURI(uri);
        mongoClient = new MongoClient(clientURI);
        readXMLFile = new ReadXMLFile(mongoClient,doc);

    }

    //----------------------CREATE----------------------------

    public void createDB(String DBName) {

        mongoDatabase = mongoClient.getDatabase(DBName);
        db = doc.createElement("DataBase");
        db.setAttribute("dataBaseName", DBName);
        currentDBName = DBName;
        rootElement.appendChild(db);
        //setCurrentDBName(DBName);
        trans();
        System.out.println("DB CREATED");
    }



    public void createTable( String tableName, ArrayList<ArrayList<String>> attr) {
        //header
        table = doc.createElement("Table");

        table.setAttribute("tableName", tableName);

        structure = doc.createElement("Structure");
        trans();

        //table's elements
        String[] val = {"name", "isNull", "type", "length"};

        for (ArrayList<String> strings : attr) {
            Element attribute = doc.createElement("Attribute");
            for (int i = 0; i < val.length; i++) {
                System.out.println("Atr details: " + val[i] + ", " + strings);
                attribute.setAttribute(val[i], strings.get(i));
            }
            structure.appendChild(attribute);
        }
        table.appendChild(structure);
        db.appendChild(table);
        //collection for data
        mongoDatabase.createCollection(tableName);
        trans();
    }

    public void createFK(String tableName, String[] fk) {
        Element foreignKey = doc.createElement("foreignKey");

        String[] val = {"refTable", "refAttribute"};
        String[] attr;

        int i =0;
        while (i < fk.length) {
            Element e = doc.createElement("fkAttribute");
            int x=i;
            //newIndex(tableName,fk[i]);
            i++;
            for (String s : val) {
                e.setAttribute(s, fk[i]);
                i++;
            }
            e.appendChild(doc.createTextNode(fk[x]));
            foreignKey.appendChild(e);
        }
        Node e = readXMLFile.getTable(currentDBName,tableName);
        e.appendChild(foreignKey);
        trans();
    }

    public void createPK(String tableName, String[] pk){
        Element primaryKey = doc.createElement("primaryKey");
        int i ;
        for (i = 0; i < pk.length; i += 3) {
            Element e = doc.createElement("pkAttribute");
            e.appendChild(doc.createTextNode(pk[i]));
            primaryKey.appendChild(e);
        }
        Node e = readXMLFile.getTable(currentDBName,tableName);
        e.appendChild(primaryKey);
        trans();
    }

    public void createIndex(String[] indexes, String tableName) {
        String str;
        Node tableN = readXMLFile.getTable(currentDBName, tableName);
        MongoCollection<Document> collection = mongoDatabase.getCollection(tableName);
        Element in = doc.createElement("indexFiles");

        //String indexString = "{ ";
        //DBObject obj = (DBObject) new BasicBSONList();

        System.out.println("table: " + tableName);
        for (String index : indexes) {
            String value = "";
            String key = "";
            newIndex(tableName, index);
            collection.createIndex(Indexes.ascending(index));
            Element e = doc.createElement("index");
            e.appendChild(doc.createTextNode(index));
            in.appendChild(e);
        }

        tableN.appendChild(in);
        trans();
    }

    public void newIndex(String tableName, String attribute){
        MongoCollection<Document> collection = mongoDatabase.getCollection(tableName);
        String indexName = tableName+'.'+attribute+".ind";
        mongoDatabase.createCollection(indexName);
        MongoCollection<Document> indexCollection = mongoDatabase.getCollection(indexName);
        if(readXMLFile.isUnique(currentDBName,tableName, attribute) || readXMLFile.isFk(currentDBName,tableName, attribute)){
            //FindIterable<Document> rows = collection.find();
            try (MongoCursor<Document> cursor = collection.find().iterator()) {
                while (cursor.hasNext()) {
                    Document str = cursor.next();
                    addIndex(tableName, attribute, readXMLFile.getAttributeValue(currentDBName, tableName, attribute, str.get("value").toString()), str.get("_id").toString());
                }
            }
        }else {
            System.out.println("Not fk");
            if(!readXMLFile.isUnique(currentDBName,tableName, attribute)){
                try (MongoCursor<Document> cursor = collection.find().iterator()) {
                    while (cursor.hasNext()) {
                        Document str = cursor.next();
                        System.out.println(attribute);

                        String val = readXMLFile.getAttributeValue(currentDBName, tableName, attribute, str.get("value").toString());
                        System.out.println("_id: " + val + "\n" + "value: " + str.get("_id"));
                        addIndex(tableName, attribute, val, str.get("_id").toString());

                    }
                }
            }
        }
    }

    public void addIndex(String tableName, String attribute, String id, String value){
        String indexFileName = tableName+'.'+attribute+".ind";
        MongoCollection<org.bson.Document> collection = mongoDatabase.getCollection(indexFileName);
        long exists;
        if(readXMLFile.checkType(currentDBName, tableName, attribute, "int")){
            exists = collection.countDocuments(eq("_id", Integer.parseInt(id)));
        }else {
            if(readXMLFile.checkType(currentDBName, tableName, attribute, "float")){
                exists = collection.countDocuments(eq("_id", Float.parseFloat(id)));
            } else {
                exists = collection.countDocuments(eq("_id", id));
            }
        }
        if(exists == 0) {
            System.out.println("NEW DOC");
            Document document = new Document();

            if(readXMLFile.checkType(currentDBName, tableName, attribute, "int")){
                document.append("_id", Integer.parseInt(id));
            }else {
                if(readXMLFile.checkType(currentDBName, tableName, attribute, "float")){
                    document.append("_id", Float.parseFloat(id));
                } else {
                    document.append("_id", id);
                }
            }
            document.append("value", value);
            collection.insertOne(document);
        }
        else{
            System.out.println("OLD DOC");
            FindIterable<Document> iterable;
            if(readXMLFile.checkType(currentDBName, tableName, attribute, "int")){
                 iterable = collection.find(eq("_id", Integer.parseInt(id)));
            }else {
                if(readXMLFile.checkType(currentDBName, tableName, attribute, "float")){
                     iterable = collection.find(eq("_id", Float.parseFloat(id)));
                } else {
                    iterable = collection.find(eq("_id", id));
                }
            }


            try (MongoCursor<Document> cursor = iterable.iterator()) {
                while (cursor.hasNext()) {
                    Document tempC = cursor.next();
                    String newValue = tempC.get("value") + "#" + value;
                    BasicDBObject updateFields = new BasicDBObject();
                    updateFields.append("value", newValue);

                    BasicDBObject setQuery = new BasicDBObject();
                    setQuery.append("$set", updateFields);

                    BasicDBObject searchQuery;

                    if (readXMLFile.checkType(currentDBName, tableName, attribute, "int")) {
                        searchQuery = new BasicDBObject("_id", Integer.parseInt(id));
                    } else {
                        if (readXMLFile.checkType(currentDBName, tableName, attribute, "float")) {
                            searchQuery = new BasicDBObject("_id", Float.parseFloat(id));
                        } else {
                            searchQuery = new BasicDBObject("_id", id);
                        }
                    }
                    //System.out.println("SEARCHING FOR ID: " + id);

                    collection.updateOne(searchQuery, setQuery);
                }
            }
        }
    }


    public void uniqueConstraint(String[] uq, String tableName){
        Element unique = doc.createElement("uniqueKeys");
        MongoCollection<Document> collection = mongoDatabase.getCollection(tableName);
        //System.out.println(tableName);
        int i ;

        for (i = 0; i < uq.length; i += 3) {
            Element u = doc.createElement("uniqueAttribute");
            u.appendChild(doc.createTextNode(uq[i]));
            Document index = new Document(uq[i], 1);
            collection.createIndex(index, new IndexOptions().unique(true));
            unique.appendChild(u);
        }

        table.appendChild(unique);
        trans();

    }

    //-------------------DROP----------------------------------

    public void dropTable(String tbName) {
        //drop structure
        Node dropTb = readXMLFile.getTable(currentDBName,tbName);
        Node DB = readXMLFile.getDataBase(currentDBName);
//        System.out.println(dropTb.getNodeName());
//        System.out.println(DB.getNodeName());
//        System.out.println(db.getTagName());
        DB.removeChild(dropTb);
        trans();
        //drop data
        MongoCollection<Document> collection = mongoDatabase.getCollection(tbName);
        collection.drop();
        ArrayList<String> indexes = readXMLFile.getIndexFiles(currentDBName, tbName);
        if(indexes != null ) {
            for (String index : indexes) {
                MongoCollection<Document> coll = mongoDatabase.getCollection(tbName + "." + index + ".ind");
                coll.drop();
            }
        }
    }

    public void dropDatabase(String dbName) {
        //mongoClient.getDatabase(currentDBName).drop();

        NodeList tables = readXMLFile.getDataBase(dbName).getChildNodes();

        for (int x = 0; x < tables.getLength(); x++){
            System.out.println("Drop table: " + tables.item(x).getAttributes().getNamedItem("tableName").getNodeValue());
            MongoCollection<Document> collection = mongoDatabase.getCollection(tables.item(x).getAttributes().getNamedItem("tableName").getNodeValue());
            System.out.println("Drop colldocs: " + collection.countDocuments());
            collection.drop();
            //dropTable(tables.item(x).getAttributes().getNamedItem("tableName").getNodeValue());
        }
        ArrayList<String> indexes = readXMLFile.getAllIndexFiles(currentDBName);
        for (String index : indexes) {
            MongoCollection<Document> collection = mongoDatabase.getCollection(index);
            collection.drop();
        }
        Node dropDB = readXMLFile.getDataBase(dbName);
        rootElement.removeChild(dropDB);
        trans();
    }

    //-----------------INSERT-DELETE ROW----------------

    public void insert(String tableName, ArrayList<String> newData, int n){

        MongoCollection<Document> collection = mongoDatabase.getCollection(tableName);
        Document document = new Document();
        ArrayList<String> attributes = readXMLFile.getAllAttributes(currentDBName,tableName);

        int pkAtr=0;
        //search for pkAttribute's index
        for(int i = 0; i < attributes.size(); i++){
            if(readXMLFile.isPk(currentDBName, tableName, attributes.get(i))){
                pkAtr = i;
                break;
            }
        }
        StringBuilder value= new StringBuilder();

        boolean ok = true;
        try {
            //going trough each attribute's value
            for (int i = 0; i < newData.size() && i < attributes.size(); i++) {

                //checking the constraints
//                System.out.println(attributes.get(i));
//                System.out.println(newData.get(i));

                if(!readXMLFile.fkConstraintInsert(currentDBName,tableName,attributes.get(i), newData.get(i)) ){
                    System.out.println("Foreign key constraint failed!");
                    ok = false;
                    break;
                }
                if(!readXMLFile.pkConstraint(currentDBName,tableName,attributes.get(i), newData.get(i))) {
                    System.out.println("Primary key already exists!");
                    ok = false;
                    break;
                }

                //if current attribute is the primary key
                if(i==pkAtr){
                    //check id's type
                    if(readXMLFile.checkType(currentDBName, tableName, attributes.get(i), "int")){
                        document.append("_id", Integer.parseInt(newData.get(i)));
                    }else {
                        if(readXMLFile.checkType(currentDBName, tableName, newData.get(i), "float")){
                            document.append("_id", Float.parseFloat(newData.get(i)));
                        } else {
                            document.append("_id", newData.get(i));
                        }
                    }

                }else{
                    value.append(newData.get(i)).append("#");
                }

                if(readXMLFile.isIndex(currentDBName, tableName, attributes.get(i))){
                    addIndex(tableName,attributes.get(i),newData.get(i),newData.get(pkAtr));
                }
            }
            if(ok) {
                document.append("value", value.toString());
                collection.insertOne(document);
                System.out.println("Successfully inserted");
            }
        }catch (MongoWriteException e){
            System.out.println(e.getMessage());
        }
    }

    public void delete(String tableName, String attr, String value){
        MongoCollection<Document> collection = mongoDatabase.getCollection(tableName);
        if(readXMLFile.isPk(currentDBName, tableName, attr)){
            attr = "_id";
        }
        FindIterable<Document> row = collection.find(eq(attr, value));

        Node ind = readXMLFile.getTableAttr(currentDBName, tableName, "indexFiles");
        NodeList indexes;
        if(ind!=null){
            indexes = ind.getChildNodes();
        }else {
            indexes = null;
        }

        for(Document l : row) {
            if (!readXMLFile.fkConstraintDelete(currentDBName, tableName, l)) {
                System.out.println("Cannot delete FK constraint failed!");
            }else {
                if (indexes != null) {
                    for( int x = 0; x < indexes.getLength(); x++){
                        //get indexFile
                        MongoCollection<Document> indexCollection = mongoDatabase.getCollection(tableName+"."+indexes.item(x).getTextContent()+".ind");
                        System.out.println("IndexAttr: " + indexes.item(x).getTextContent());
                        System.out.println("Id in this doc: " + l.get("_id").toString());

                        //get indexAttribute value
                        String indexValue = readXMLFile.getAttributeValue(currentDBName, tableName, indexes.item(x).getTextContent(), l.get("value").toString());
                        System.out.println("_id: "+ indexValue);

                        Document delDocs =  indexCollection.find(eq("_id", indexValue)).first();
                        assert delDocs != null;
                        String[] indexVal = delDocs.get("value").toString().split("#");
                        StringBuilder newVal = new StringBuilder();
                        for (String s : indexVal) {
                            if (!s.equals(l.get("_id").toString())) {
                                newVal.append(s).append("#");
                            }
                        }

                        if(newVal.toString().isEmpty()){
                            indexCollection.deleteOne(eq("_id", indexValue));
                        }else {

                            BasicDBObject updateFields = new BasicDBObject();
                            updateFields.append("value", newVal.toString());

                            BasicDBObject setQuery = new BasicDBObject();
                            setQuery.append("$set", updateFields);

                            BasicDBObject searchQuery = new BasicDBObject("_id", indexValue);

                            indexCollection.updateOne(searchQuery, setQuery);
                        }

                    }
                }
                collection.deleteOne(eq(attr, value));
            }
        }

    }
    public Bson getFilter(ArrayList<String> condition, String tableName, String attribute){
        Bson filter = null;
        switch (condition.get(1)) {
            case "=":
                if (readXMLFile.checkType(currentDBName, tableName, attribute, "int")) {
                    filter = (eq("_id", Integer.parseInt(condition.get(2))));
                } else {
                    if (readXMLFile.checkType(currentDBName, tableName, attribute, "float")) {
                        filter = eq("_id", Float.parseFloat(condition.get(2)));
                    } else {
                        filter = eq("_id", condition.get(2));
                    }
                }
                break;
            case "<=":
                filter = (lte("_id", Integer.parseInt(condition.get(2))));
                break;
            case ">=":
                filter = (gte("_id", Integer.parseInt(condition.get(2))));
                break;
            case "<":
                filter = (lt("_id", Integer.parseInt(condition.get(2))));
                break;
            case ">":
                filter = (gt("_id", Integer.parseInt(condition.get(2))));
                break;

        }
        return filter;
    }

    public HashMap<String, Sets<Integer>>  where(ArrayList<ArrayList<String>> conditions){

        if (conditions == null)
            return null;
        System.out.println("nr of conditions: " + conditions.size());
        HashMap<String, Sets<Integer>> conditionIDs = new HashMap<>();

        for(ArrayList<String> condition: conditions){
            String table = condition.get(0).split("\\.")[0];
            Sets<Integer> tempSet = oneCondition(condition,conditionIDs.get(table));
            System.out.println(tempSet.length());
            conditionIDs.put(table, tempSet);

        }
        System.out.println("WHERE DONE");
        return conditionIDs;

    }

    //checks one where condition and returns a set of ids which satisfies it
    public Sets<Integer> oneCondition(ArrayList<String> condition, Sets<Integer> ids){
        String tableName = condition.get(0).split("\\.")[0];
        String attribute = condition.get(0).split("\\.")[1];
        Sets<Integer> set = new Sets<>();
        MongoCollection<Document> collection = mongoDatabase.getCollection(tableName);

        if(ids == null) {
            System.out.println("First condition on this tabel");
            if(readXMLFile.isIndex(currentDBName, tableName,attribute)) {
                MongoCollection<Document> indexCollection = mongoDatabase.getCollection(tableName + "." + attribute + ".ind");
                Bson filter = getFilter(condition, tableName, attribute);

                FindIterable<Document> iterable = indexCollection.find(filter);

                try (MongoCursor<Document> cursor = iterable.iterator()) {
                    while (cursor.hasNext()) {

                        Document tempDoc = cursor.next();
                        String[] values = tempDoc.get("value").toString().split("#");
                        for (String value : values) {
                            set.add(Integer.parseInt(value));
                        }
                    }
                }

            } else {
                FindIterable<Document> iterable = collection.find();

                try (MongoCursor<Document> cursor = iterable.iterator()) {
                    while (cursor.hasNext()) {
                        Document tempDoc = cursor.next();
                        if (readXMLFile.isPk(currentDBName, tableName, attribute)) {
                            condition.set(0, tempDoc.get("_id").toString());
                        } else {
                            condition.set(0, readXMLFile.getAttributeValue(currentDBName, tableName, attribute, tempDoc.get("value").toString()));
                        }
                        if (checkFilter(condition)) {
                            set.add(Integer.parseInt(tempDoc.get("_id").toString()));
                        }
                    }
                }
            }
        } else {
            System.out.println("Not the first condition on this tabel");
            for (int i = 0; i < ids.length(); i++) {
                if(readXMLFile.isPk(currentDBName, tableName, attribute)){
                    condition.set(0, ids.get(i).toString());
                }else {
                    Document tempDoc = collection.find(eq("_id", ids.get(i))).first();
                    assert tempDoc != null;
                    condition.set(0, readXMLFile.getAttributeValue(currentDBName, tableName, attribute, tempDoc.get("value").toString()));
                }
                if (checkFilter(condition)) {
                    set.add(Integer.parseInt(ids.get(i).toString()));
                }
            }
        }

        return set;
    }


    public boolean checkFilter(ArrayList<String> constrains){
        boolean out=false;

        switch (constrains.get(1)){
            case "=":
                out = constrains.get(0).equals(constrains.get(2));
                break;
            case "<=":
                out = Double.parseDouble(constrains.get(0) )<= Double.parseDouble(constrains.get(2));
                break;
            case ">=":
                out = Integer.parseInt(constrains.get(0) ) >= Double.parseDouble(constrains.get(2));
                break;
            case "<":
                out = Double.parseDouble(constrains.get(0) ) < Double.parseDouble(constrains.get(2));
                break;
            case ">":
                out = Double.parseDouble(constrains.get(0) ) > Double.parseDouble(constrains.get(2));
                break;
        }
        //System.out.println(constrains.get(0) +  " " + constrains.get(1) + " " + constrains.get(2) + " : " + out );
        return out;
    }

    public int select( ArrayList<String> projectionAttributes, String from, ArrayList<ArrayList<String>> joinConditions, ArrayList<ArrayList<String>> whereConditions, ArrayList<String> groupBy, ArrayList<ArrayList<String>> having) throws FileNotFoundException, UnsupportedEncodingException {

        pw = new PrintWriter("temp.txt", "UTF-8");
        pw.print("");
        finalIDs.clear();
        int attributeWhichHasIndex = 0;
        int numberOfAggregationFuncs = 0;

        if(projectionAttributes.get(0).equals("*")){
            ArrayList<String> tableNames = new ArrayList<>();
            projectionAttributes.remove(0);
            tableNames.add(from);
            if(joinConditions!=null) {
                for (ArrayList<String> joinCondition : joinConditions) {
                    tableNames.add(joinCondition.get(0));
                }
            }

            for(String table : tableNames){
               ArrayList<String> currentTableAttributes =  readXMLFile.getAllAttributes(currentDBName, table);
               for(String currentAttributes: currentTableAttributes){
                 projectionAttributes.add(table+"."+currentAttributes);
               }
            }
        } else {
            int grouping = 0;
            for(String projectionAttribute: projectionAttributes){
                System.out.println("*"+projectionAttribute);

                if(projectionAttribute.contains("(")){
                    numberOfAggregationFuncs ++;
                } else {
                    if(groupBy != null && groupBy.contains(projectionAttribute)){
                        //System.out.println("Column " +  attr + " is invalid in the select list because it is not contained in either an aggregate function or the GROUP BY clause.");
                        //return -1;
                        grouping++;
                    }
                    String tableName = projectionAttribute.split("\\.")[0];
                    if (!readXMLFile.isIndex(currentDBName, tableName, projectionAttribute)) {
                        attributeWhichHasIndex ++;
                    }
                }
            }

            if(numberOfAggregationFuncs != 0 && numberOfAggregationFuncs != projectionAttributes.size() && groupBy == null){
                System.out.println("There are non-aggregation functions in the projection so you must define group by!");
            }else {
                if(numberOfAggregationFuncs != 0 && grouping != projectionAttributes.size()-numberOfAggregationFuncs){
                    System.out.println("Each attribute in the projection must be present in the group by!");
                    System.out.println(grouping);
                    System.out.println(numberOfAggregationFuncs);
                }
            }

        }

        if(having != null && groupBy == null){
            System.out.println("You can't use having without group by.");
            return -1;
        }
        MongoCollection<Document> collection = mongoDatabase.getCollection(from);

        if (joinConditions == null && whereConditions == null && having == null && groupBy == null){
            if (attributeWhichHasIndex == projectionAttributes.size()) {

                System.out.println("NO JOIN WITHOUT WHERE BUT EVERYONE HAS INDEX");
                MongoCollection<Document> indexAttribute = mongoDatabase.getCollection(from + "." + projectionAttributes.get(0) + ".ind");
                FindIterable<Document> iterable = indexAttribute.find();


                try (MongoCursor<Document> cursor = iterable.iterator()) {
                    while (cursor.hasNext()) {
                        Document tempC = cursor.next();

                        String[] value = tempC.get("value").toString().split("#");

                        if (projectionAttributes.size() > 1) {
                            for (String s : value) {
                                System.out.println(projectionAttributes.get(0) + ": " + tempC.get("_id"));
                                for (int j = 1; j < projectionAttributes.size(); j++) {
                                    Document collDoc = collection.find(eq("_id", s)).first();
                                    assert collDoc != null;
                                    readXMLFile.getAttributeValue(currentDBName, from, projectionAttributes.get(j), collDoc.get("value").toString());
                                    pw.println(projectionAttributes.get(j) + ": " + tempC.get("_id"));
                                }
                                pw.println();
                            }
                        } else {//Only one indexFile is needed! It works when no other is required.
                            pw.println(projectionAttributes.get(0) + ": " + tempC.get("_id"));
                            pw.println(tempC.get("value"));
                            pw.println();
                        }
                    }
                }
            } else {
                System.out.println("NO JOIN NO WHERE NO INDEX");
                FindIterable<Document> iterable = collection.find();

                try (MongoCursor<Document> cursor = iterable.iterator()) {
                    while (cursor.hasNext()) {
                        Document tempC = cursor.next();

                        String[] value = tempC.get("value").toString().split("#");
                        System.out.println(projectionAttributes.size());
                        if (projectionAttributes.size() > 1) {
                            for (int i = 0; i < value.length; i++) {
                                pw.println(projectionAttributes.get(0) + ": " + tempC.get("_id"));

                                for (int j = 1; j < projectionAttributes.size(); j++) {
                                    readXMLFile.getAttributeValue(currentDBName, from, projectionAttributes.get(j), tempC.get("value").toString());
                                    pw.println(projectionAttributes.get(j) + ": " + tempC.get("_id"));
                                }
                                pw.println();
                            }
                        } else {
                            pw.println(projectionAttributes.get(0) + ": " + tempC.get("_id"));
                            pw.println(tempC.get("value"));
                            pw.println();
                        }
                    }
                }
                }
        } else {
            ArrayList<Integer> idList = new ArrayList<>();
            //where
            HashMap<String, Sets<Integer>> conditionSatisfyingIDs;
            conditionSatisfyingIDs = where(whereConditions);

            //join
            if (joinConditions != null) {
                join(joinConditions, conditionSatisfyingIDs);
            } else {
                if(whereConditions!=null) {
                    Sets<Integer> tempSet = conditionSatisfyingIDs.get(from);
                    int length = tempSet.length();
                    ArrayList<Integer> aux = new ArrayList<>();
                    for (int i = 0; i < length; i++) {
                        aux.add(tempSet.get(i));
                    }
                    finalIDs.put(from, aux);
                } else {
                    FindIterable<Document> iterable = collection.find();
                    ArrayList<Integer> ids = new ArrayList<>();
                    try (MongoCursor<Document> cursor = iterable.iterator()) {
                        while (cursor.hasNext()) {
                            Document tempC = cursor.next();
                            ids.add(Integer.parseInt(tempC.get("_id").toString()));

                        }
                    } finally {
                        finalIDs.put(from, ids);
                    }
                }
            }

            groupBY(groupBy);
            boolean gr = groupBy != null;
            if(groupBy==null && numberOfAggregationFuncs == projectionAttributes.size()){
                int length = finalIDs.get(from).size();
                ArrayList<Integer> initialGroups = new ArrayList<>(Collections.nCopies(length, 1));
                finalIDs.put("groupBy", initialGroups);
                gr = true;
            }
            evaluateAggregations(projectionAttributes);
            projectionFromHash(projectionAttributes,gr);
            pw.flush();
            //having
            //groupBy
            //projection
        }


        /*
        if (joinConditions != null) {
            HashMap<String, Sets<Integer>> whereConditions = null;

            if (conditions != null) {
                System.out.println("JOIN WITH WHERE");
                whereConditions = where(conditions);
            } else {
                System.out.println("JOIN WITHOUT WHERE");
            }
            finalIDs = join(joinConditions, whereConditions);

            projectionFromHash(finalIDs, projectionAttributes);

        } else {
            MongoCollection<Document> collection = mongoDatabase.getCollection(from);
            String tableName = from;

            if (conditions != null) { //no join but there are some where conditions
                System.out.println("WHERE WITHOUT JOIN");
                FindIterable<Document> iterable = collection.find();
                MongoCursor<Document> cursor = iterable.iterator();
                HashMap<String, Sets<Integer>> whereConditions = null;
                if (conditions != null) {
                    whereConditions = where(conditions);
                }


                try {
                    while (cursor.hasNext()) {
                        Document tempDoc = cursor.next();
                        int currentID =Integer.parseInt(tempDoc.get("_id").toString());
                        if(whereConditions.get(tableName).contains(currentID)) {
                            idList.add(currentID);
                        }
                    }
                    System.out.println("WHERE NO JOIN");
                    finalIDs.put(tableName, idList);
                    projectionFromHash(finalIDs, projectionAttributes);
                } finally {
                    cursor.close();
                }
            } else {
                boolean everyOneHasIndex = true;
                for (String projectionAttribute : projectionAttributes) {
                    if (!readXMLFile.isIndex(currentDBName, tableName, projectionAttribute)) {
                        everyOneHasIndex = false;
                        break;
                    }
                }
                if (everyOneHasIndex) {

                    System.out.println("NO JOIN WHITOUT WHERE BUT EVERYONE HAS INDEX");
                    MongoCollection<Document> indexAttribute = mongoDatabase.getCollection(tableName + "." + projectionAttributes.get(0) + ".ind");
                    FindIterable<Document> iterable = indexAttribute.find();



                    MongoCursor<Document> cursor = iterable.iterator();
                    try {
                        while (cursor.hasNext()) {
                            Document tempC = cursor.next();

                            String[] value = tempC.get("value").toString().split("#");
                            System.out.println(projectionAttributes.size());
                            if (projectionAttributes.size() > 1) {
                                for (int i = 0; i < value.length; i++) {
                                   // System.out.println(projectionAttributes.get(0) + ": " + tempC.get("_id"));
                                    //for (int j = 1; j < projectionAttributes.size(); j++) {
                                     //   Document collDoc = collection.find(eq("_id", value[i])).first();
                                        idList.add(Integer.parseInt(value[i]));
                                     //   readXMLFile.getAttributeValue(currentDBName, tableName, projectionAttributes.get(j), collDoc.get("value").toString());
                                     //   System.out.println(projectionAttributes.get(j) + ": " + tempC.get("_id"));
                                   // }
                                   // System.out.println();
                                }
                            } else {//Only one indexFile is needed! It works when no other is required.
                                System.out.println(projectionAttributes.get(0) + ": " + tempC.get("_id"));
                                System.out.println(tempC.get("value"));
                                System.out.println();
                            }
                        }
                    } finally {
                        cursor.close();
                    }
                } else {
                    System.out.println("NO JOIN NO WHERE NO INDEX");
                    FindIterable<Document> iterable = collection.find();

                    MongoCursor<Document> cursor = iterable.iterator();
                    try {
                        while (cursor.hasNext()) {
                            Document tempC = cursor.next();

                            String[] value = tempC.get("value").toString().split("#");
                            System.out.println(projectionAttributes.size());
                            if (projectionAttributes.size() > 1) {
                               // for (int i = 0; i < value.length; i++) {
                                    /*System.out.println(projectionAttributes.get(0) + ": " + tempC.get("_id"));

                                    for (int j = 1; j < projectionAttributes.size(); j++) {
                                        readXMLFile.getAttributeValue(currentDBName, tableName, projectionAttributes.get(j), tempC.get("value").toString());
                                        System.out.println(projectionAttributes.get(j) + ": " + tempC.get("_id"));
                                    }
                                    System.out.println();
                                    idList.add(Integer.parseInt(tempC.get("_id").toString()));
                               // }
                            } else {
                                System.out.println("itt");
                                System.out.println(projectionAttributes.get(0) + ": " + tempC.get("_id"));
                                System.out.println(tempC.get("value"));
                                System.out.println();
                            }
                        }
                    } finally {
                        cursor.close();
                    }
                }
            }
        }
*/
        System.out.println("DONE");
        return 0;

    }

    //projection when the finalIDS are in a hashmap/ join with or without where condition
    public void projectionFromHash(ArrayList<String> projectionAttributes, boolean grouping){

        Iterator it = finalIDs.entrySet().iterator();
        HashMap.Entry pair = (HashMap.Entry) it.next();
        long length = ((ArrayList<Integer>) pair.getValue()).size();

        System.out.println("Length: " + length);
        if(grouping) {
            int prevGroup = -1;
            for (int i = 0; i < length; i++) {
                int groupNumber = finalIDs.get("groupBy").get(i);
                if(groupNumber!=prevGroup) {
                    for (String projectionAttribute : projectionAttributes) {

                        if (!projectionAttribute.contains("(")) {
                            String tableName = projectionAttribute.split("\\.")[0];
                            String attributeName = projectionAttribute.split("\\.")[1];
                            ArrayList<Integer> id = finalIDs.get(tableName);
                            MongoCollection<Document> collection = mongoDatabase.getCollection(tableName);
                            Document tempDoc = collection.find(eq("_id", id.get(i))).first();
                            if (!readXMLFile.isPk(currentDBName, tableName, attributeName)) {
                                assert tempDoc != null;
                                pw.println(groupNumber + " -v " + attributeName + ": " + readXMLFile.getAttributeValue(currentDBName, tableName, attributeName, tempDoc.get("value").toString()));
                            } else {
                                assert tempDoc != null;
                                pw.println(groupNumber + " -i " + attributeName + ": " + tempDoc.get("_id").toString());

                            }
                        } else {
                            pw.println(groupNumber + " -a " + projectionAttribute + ": " + aggregationValues.get(projectionAttribute).get(groupNumber-1));
                        }
                        prevGroup = groupNumber;
                    }
                    pw.println();

                }
            }
        } else {
            for (int i = 0; i < length; i++) {
                for (String projectionAttribute : projectionAttributes) {

                    String tableName = projectionAttribute.split("\\.")[0];
                    String attributeName = projectionAttribute.split("\\.")[1];
                    ArrayList<Integer> id = finalIDs.get(tableName);
                    MongoCollection<Document> collection = mongoDatabase.getCollection(tableName);
                    int idV = id.get(i);
                    //System.out.println("--"+idV);
                    Document tempDoc = collection.find(eq("_id",idV )).first();
                    if (!readXMLFile.isPk(currentDBName, tableName, attributeName)) {
                        assert tempDoc != null;
                        pw.println(attributeName + ": " + readXMLFile.getAttributeValue(currentDBName, tableName, attributeName, tempDoc.get("value").toString()));
                    } else {
                        assert tempDoc != null;
                        pw.println(attributeName + ": " + tempDoc.get("_id").toString());

                    }

                }
                //System.out.println();
                pw.println();
            }
        }
        System.out.println("PROJECTION DONE");
    }


    //-----------EVALUATE AGGREGATION FUNCTIONS-----------------------------------------------


    public  void evaluateAggregations(ArrayList<String> projectionAttributes){
        System.out.println("Aggregate function evaluation started");
        for(String projectionAttribute: projectionAttributes){
            String[] temp = projectionAttribute.split("\\(");
            if(temp.length > 0 ){
                System.out.println(projectionAttribute);
                String aggregationType = temp[0];
                switch (aggregationType){
                    case "count": {
                        count(projectionAttribute);
                        break;
                    }
                    case "min": {
                        minValue(projectionAttribute);
                        break;
                    }
                    case "max": {
                        maxValue(projectionAttribute);
                        break;
                    }
                    case "avg": {
                        average(projectionAttribute);
                        break;
                    }
                    case "sum": {
                        sum(projectionAttribute);
                        break;
                    }

                }
            }

        }
        System.out.println("AGGREGATION FUNCTIONS DONE");
    }


    public void count(String aggregationAttribute){
        String temp = aggregationAttribute.split("\\(")[1].split("\\)")[0];
        String tableName = temp.split("\\.")[0];
        String attribute = temp.split("\\.")[1];
        MongoCollection<Document> collection = mongoDatabase.getCollection(tableName);
        ArrayList<Integer> groupNumbers = finalIDs.get("groupBy");
        ArrayList<Integer> tableIds = finalIDs.get(tableName);
        ArrayList<Double> aggVal = new ArrayList<>();
        int length = groupNumbers.size();

        double countRow = 0;
        int actGroupNumber = groupNumbers.get(0);
        for (int i = 0; i < length; i++) {
            if(groupNumbers.get(i)!= actGroupNumber){
                aggVal.add(countRow);
                countRow = 0;
            } else {
                if(!aggregationAttribute.equals("*")) {
                    int id = tableIds.get(i);
                    if(readXMLFile.getAttributeValue(currentDBName, tableName, attribute, Objects.requireNonNull(collection.find(eq("_id", id)).first()).get("value").toString()) != null) {
                        countRow ++;
                    }
                } else {
                    countRow++;
                }
            }
        }
        aggVal.add(countRow);
        aggregationValues.put(aggregationAttribute, aggVal);

    }

    public void minValue(String aggregationAttribute){
        String temp = aggregationAttribute.split("\\(")[1].split("\\)")[0];
        String tableName = temp.split("\\.")[0];
        String attribute = temp.split("\\.")[1];
        MongoCollection<Document> collection = mongoDatabase.getCollection(tableName);
        ArrayList<Integer> groupNumbers = finalIDs.get("groupBy");
        ArrayList<Integer> tableIds = finalIDs.get(tableName);
        ArrayList<Double> aggVal = new ArrayList<>();
        int length = groupNumbers.size();

        int id = tableIds.get(0);
        String value = Objects.requireNonNull(collection.find(eq("_id", id)).first()).get("value").toString();
        double minVal = Integer.parseInt(readXMLFile.getAttributeValue(currentDBName, tableName, attribute, value));
        int actGroupNumber = groupNumbers.get(0);
        double currentVal;
        for (int i = 0; i < length; i++) {
            id = tableIds.get(i);
            value = collection.find(eq("_id", id)).first().get("value").toString();
            currentVal = Integer.parseInt(readXMLFile.getAttributeValue(currentDBName, tableName, attribute, value));

            if(groupNumbers.get(i)!= actGroupNumber){
                aggVal.add(minVal);
                minVal = currentVal;
            } else {
                if(currentVal < minVal){
                    minVal = currentVal;
                }
            }
        }
        aggVal.add(minVal);
        aggregationValues.put(aggregationAttribute, aggVal);

    }

    public void maxValue(String aggregationAttribute){
        String temp = aggregationAttribute.split("\\(")[1].split("\\)")[0];
        String tableName = temp.split("\\.")[0];
        String attribute = temp.split("\\.")[1];
        MongoCollection<Document> collection = mongoDatabase.getCollection(tableName);
        ArrayList<Integer> groupNumbers = finalIDs.get("groupBy");
        ArrayList<Integer> tableIds = finalIDs.get(tableName);
        ArrayList<Double> aggVal = new ArrayList<>();
        int length = groupNumbers.size();

        int id = tableIds.get(0);
        String value = collection.find(eq("_id", id)).first().get("value").toString();
        double maxVal = Integer.parseInt(readXMLFile.getAttributeValue(currentDBName, tableName, attribute, value));
        int actGroupNumber = groupNumbers.get(0);
        double currentVal;

        for (int i = 0; i < length; i++) {
            id = tableIds.get(i);
            value = collection.find(eq("_id", id)).first().get("value").toString();
            currentVal = Integer.parseInt(readXMLFile.getAttributeValue(currentDBName, tableName, attribute, value));

            if(groupNumbers.get(i)!= actGroupNumber){
                aggVal.add(maxVal);
                maxVal = currentVal;
            } else {
                if(currentVal > maxVal){
                    maxVal = currentVal;
                }
            }
        }
        aggVal.add(maxVal);
        aggregationValues.put(aggregationAttribute, aggVal);

    }

    public void sum(String aggregationAttribute){
        String temp = aggregationAttribute.split("\\(")[1].split("\\)")[0];
        String tableName = temp.split("\\.")[0];
        String attribute = temp.split("\\.")[1];
        MongoCollection<Document> collection = mongoDatabase.getCollection(tableName);
        ArrayList<Integer> groupNumbers = finalIDs.get("groupBy");
        ArrayList<Integer> tableIds = finalIDs.get(tableName);
        ArrayList<Double> aggVal = new ArrayList<>();
        int length = groupNumbers.size();

        int id;
        String value;
        double sum = 0;
        int actGroupNumber = groupNumbers.get(0);
        double currentVal;

        for (int i = 0; i < length; i++) {
            id = tableIds.get(i);
            value = collection.find(eq("_id", id)).first().get("value").toString();
            currentVal = Integer.parseInt(readXMLFile.getAttributeValue(currentDBName, tableName, attribute, value));

            if(groupNumbers.get(i)!= actGroupNumber){
                aggVal.add(sum);
                sum = 0;
            } else {
                sum+=currentVal;
            }
        }
        aggVal.add(sum);
        aggregationValues.put(aggregationAttribute, aggVal);

    }

    public void average(String aggregationAttribute){
        System.out.println("AVG");
        String temp = aggregationAttribute.split("\\(")[1].split("\\)")[0];
        String tableName = temp.split("\\.")[0];
        String attribute = temp.split("\\.")[1];
        MongoCollection<Document> collection = mongoDatabase.getCollection(tableName);
        ArrayList<Integer> groupNumbers = finalIDs.get("groupBy");
        ArrayList<Integer> tableIds = finalIDs.get(tableName);
        ArrayList<Double> aggVal = new ArrayList<>();
        int length = tableIds.size();

        int id;
        String value;
        double resultVal = 0;
        int actGroupNumber = groupNumbers.get(0);
        double currentVal, index =0;

        System.out.println(length);
        for (int i = 0; i < length; i++) {
            id = tableIds.get(i);
            //System.out.println(i);
            value = collection.find(eq("_id", id)).first().get("value").toString();
            currentVal = Integer.parseInt(readXMLFile.getAttributeValue(currentDBName, tableName, attribute, value));
            if(groupNumbers.get(i)!= actGroupNumber){
                aggVal.add(resultVal/index);
                resultVal = currentVal;
                index = 1;
                actGroupNumber = groupNumbers.get(i);
            } else {
                index++;
                resultVal+=currentVal;
            }
        }
        System.out.println("After avg");
        aggVal.add(resultVal/index);
        aggregationValues.put(aggregationAttribute, aggVal);

    }

    /*public ArrayList<Integer> countFunctionHash(String attributeTable, HashMap<String, ArrayList<Integer>> finalIds){
        String table = attributeTable.split("\\.")[0];
        String attribute = attributeTable.split("\\.")[1];


        ArrayList<Integer> idList = finalIds.get(table);
        int length = idList.size();

        ArrayList<Integer> countFunctionValues = new ArrayList<>();
        MongoCollection<Document> collection = mongoDatabase.getCollection(table );

        if(readXMLFile.isIndex(currentDBName,table,attribute)){//use of the indexFiles
            MongoCollection<Document> indexAttribute = mongoDatabase.getCollection(table + "." + attribute + ".ind");
            for(int i=0 ;i < length; i++){
                String value = collection.find(eq("_id",idList.get(i))).first().get("value").toString();//get the current value of the attribute
                String indexValue = indexAttribute.find(eq("_id",readXMLFile.getAttributeValue(currentDBName, table, attribute, value))).first().get("value").toString();//get the number of ids which belong to the current value
                int nr = indexValue.split("#").length;//split and count
                countFunctionValues.add(nr);
            }
        } else {
            //going through the data collection and counting the appearance of every value
            HashMap<String, Integer> tempHash = new HashMap<>();

            FindIterable<Document> iterable = collection.find();

            MongoCursor<Document> cursor = iterable.iterator();
            try {
                while (cursor.hasNext()) {
                    Document tempC = cursor.next();
                    String value = tempC.get("value").toString();
                    System.out.println(attribute+" " + value);
                    String attributeValue = readXMLFile.getAttributeValue(currentDBName, table, attribute, value);
                    if(tempHash.get(attributeValue) != null){
                        System.out.println("*" + attributeValue);
                        tempHash.put(attributeValue, tempHash.get(attribute) + 1);
                    } else {
                        tempHash.put(attributeValue, 1);
                    }
                }
            } finally {
                cursor.close();
            }

            //making an arraylist of the needed countValues
            for(int i=0 ;i < length; i++){
                String value = collection.find(eq("_id",idList.get(i))).first().get("value").toString();//get the current value of the attribute
                String attributeValue = readXMLFile.getAttributeValue(currentDBName, table, attribute, value);
                countFunctionValues.add(tempHash.get(attributeValue));
            }
        }
        return countFunctionValues;
    }*/



    //-----------SOLVES JOIN OPERATIONS-----------------------------------------------

    public void join(ArrayList<ArrayList<String>> joinTables, HashMap<String, Sets<Integer>> afterWhere){
        if(joinTables != null) {
            for (ArrayList<String> joinTable : joinTables) {
                joinTwoTables(joinTable, afterWhere);
            }
            System.out.println("JOIN DONE");
        }
    }

    public void joinTwoTables(ArrayList<String> join,  HashMap<String, Sets<Integer>> conditionIDs){

            String firstTable = join.get(1).split("\\.")[0];
            String firstAttribute = join.get(1).split("\\.")[1];
            String secondTable = join.get(2).split("\\.")[0];
            String secondAttribute = join.get(2).split("\\.")[1];
            String pkTable;
            String pkAttribute;
            String fkTable;
            String fkAttribute;
            ArrayList<ArrayList<Integer>> newIds = new ArrayList<>();

            if (readXMLFile.isPk(currentDBName, firstTable, firstAttribute)) {
                pkTable = firstTable;
                //pkAttribute = "_id";
                fkTable = secondTable;
                fkAttribute = secondAttribute;
            } else {
                pkTable = secondTable;
                //pkAttribute = "_id";
                fkTable = firstTable;
                fkAttribute = firstAttribute;
            }

            //3-different cases

            //both has an ArrayList of ids in the hashMap
            ArrayList<Integer> pkList = finalIDs.get(pkTable);
            ArrayList<Integer> fkList = finalIDs.get(fkTable);

            ArrayList<Integer> foreignKeys = new ArrayList<>();
            ArrayList<Integer> primaryKeys = new ArrayList<>();

            //fkAttribute has no values yet in the finalIDS hash
            if (fkList == null) {

                //there was some condition on the fkTable| go through only the matching ones
                if (conditionIDs != null && conditionIDs.containsKey(fkTable) ) {
                    System.out.println("First hash is empty but i have some id in the second| FK");
                    Sets<Integer> fkSet = conditionIDs.get(fkTable);

                    MongoCollection<Document> collection = mongoDatabase.getCollection(fkTable);
                    System.out.println(fkSet.length());
                    for (int i = 0; i < fkSet.length(); i++) {

                        int id = fkSet.get(i);
                        Document doc = collection.find(eq("_id", id)).first();
                        String attributeValue = readXMLFile.getAttributeValue(currentDBName, fkTable, fkAttribute, doc.get("value").toString());

                        int val = Integer.parseInt(attributeValue);
                        if(conditionIDs.get(pkTable)==null) {
                            foreignKeys.add(id);
                            primaryKeys.add(val);
                        } else {
                            if(conditionIDs.get(pkTable).contains(val)){
                                foreignKeys.add(id);
                                primaryKeys.add(val);
                            }
                        }
                    }
                } else {
                    //if there is index on the fkAttribute
                   // if(readXMLFile.isIndex(currentDBName, fkTable, fkAttribute)) {
                        MongoCollection<Document> collection = mongoDatabase.getCollection(fkTable + "." + fkAttribute + ".ind");
                        FindIterable<Document> iterable = collection.find();
                    try (MongoCursor<Document> cursor = iterable.iterator()) {
                        while (cursor.hasNext()) {
                            Document tempDoc = cursor.next();
                            int id = Integer.parseInt(tempDoc.get("_id").toString());
                            String[] values = tempDoc.get("value").toString().split("#");
                            for (String value : values) {
                                if (conditionIDs == null || conditionIDs.get(pkTable) == null ) {
                                        primaryKeys.add(id);
                                        foreignKeys.add(Integer.parseInt(value));
                                    /*primaryKeys.add(Integer.parseInt(value));
                                    foreignKeys.add(id);*/
                                } else {
                                    if (conditionIDs.get(pkTable) != null && conditionIDs.get(pkTable).contains(id)) {
                                        primaryKeys.add(id);
                                        foreignKeys.add(Integer.parseInt(value));
                                    }

                                }
                            }
                        }
                    }
                    /*} else {
                        //No index on the fkAttribute
                        MongoCollection<Document> collection = mongoDatabase.getCollection(fkTable );
                        FindIterable<Document> iterable = collection.find();
                        MongoCursor<Document> cursor = iterable.iterator();
                        try {
                            while (cursor.hasNext()) {
                                Document tempDoc = cursor.next();
                                int id = Integer.parseInt(tempDoc.get("_id").toString());
                                String values = tempDoc.get("value").toString();
                                if(conditionIDs==null) {
                                    foreignKeys.add(id);
                                    primaryKeys.add(Integer.parseInt(readXMLFile.getAttributeValue(currentDBName, fkTable, fkAttribute, values)));
                                } else {
                                    if(conditionIDs.get(pkTable)!= null && conditionIDs.get(pkTable).contains(id)){
                                        foreignKeys.add(id);
                                        primaryKeys.add(Integer.parseInt(readXMLFile.getAttributeValue(currentDBName, fkTable, fkAttribute, values)));
                                    }

                                }

                            }
                        } finally {
                            cursor.close();
                        }
                    }*/
                }
            } else {//mainHash isnt empty

                MongoCollection<Document> collection = mongoDatabase.getCollection(fkTable);
                int i = 0;
                while (i < fkList.size()) {
                    int id = fkList.get(i);
                    Document doc = collection.find(eq("_id", id)).first();
                    assert doc != null;
                    String attributeValue = readXMLFile.getAttributeValue(currentDBName, fkTable, fkAttribute, doc.get("value").toString());
                    int val = Integer.parseInt(attributeValue);
                    //check if pkAttr contains the respected value
                    if(conditionIDs!=null && conditionIDs.get(pkTable)!=null) {
                        if (conditionIDs.get(pkTable).contains(val)) {
                            primaryKeys.add(val);
                            i++;
                        } else {
                            removeIndex(i);
                        }
                    } else {
                        primaryKeys.add(val);
                        i++;
                    }
                }
                foreignKeys = finalIDs.get(fkTable);
            }
            finalIDs.put(pkTable, primaryKeys);
            finalIDs.put(fkTable, foreignKeys);

    }

    public void removeIndex(int index){
       Iterator it = finalIDs.entrySet().iterator();
        while (it.hasNext()) {
            HashMap.Entry pair = (HashMap.Entry)it.next();
            //System.out.println(pair.getKey() + " = " + pair.getValue());
            ArrayList<Integer> temp = ((ArrayList<Integer>)pair.getValue());
            if(index< temp.size()) {
                temp.remove(index);
                finalIDs.put(pair.getKey().toString(), temp);
            }
            //it.remove(); // avoids a ConcurrentModificationException
        }

    }

    public void having(ArrayList<ArrayList<String>> havingConstrains){

        ArrayList<String> aggregation = new ArrayList<>();
        for(ArrayList<String> havingConstrain: havingConstrains){
            if(finalIDs.get(havingConstrain.get(0)) == null) {
                aggregation.add(havingConstrain.get(0));
            }
        }
        if(aggregation.size() != 0) {
            evaluateAggregations(aggregation);
        }

        Iterator it = finalIDs.entrySet().iterator();
        HashMap.Entry pair = (HashMap.Entry) it.next();
        long length = ((ArrayList<Integer>) pair.getValue()).size();

        System.out.println("Length: " + length);
        for (int i = 0; i < length; i++) {
            for (ArrayList<String> havingConstrain : havingConstrains) {
                String attr = havingConstrain.get(0).split("\\(")[1].split("\\)")[0];
                String tableName = attr.split("\\.")[0];
                String attribute = attr.split("\\.")[1];
                int groupNumber = finalIDs.get("groupBy").get(i);
                havingConstrain.set(0, aggregationValues.get(attr).get(groupNumber).toString());
                if(!checkFilter(havingConstrain)){
                    removeIndex(i);
                }
            }
        }

    }

    //----------------SOLVES GROUP BY CLAUSE--------------------------------------

    public void groupBY(ArrayList<String> groupByAttributes){
        if(groupByAttributes != null) {
            int length = finalIDs.get(groupByAttributes.get(0).split("\\.")[0]).size();
            System.out.println("LN: " + length);

            ArrayList<Integer> initialGroups = new ArrayList<>(Collections.nCopies(length, 1));
            finalIDs.put("groupBy", initialGroups);
            groupByOneAttribute(groupByAttributes.get(0), 0, length);

            System.out.println(groupByAttributes.size());
            if(groupByAttributes.size() > 1 ) {
                System.out.println("MORE GROUP BY");
                for (int j = 1 ; j< groupByAttributes.size(); j++) {
                    ArrayList<Integer> temp = finalIDs.get("groupBy");
                    int startIndex = 0;
                    int gr = temp.get(0);
                    for (int i = 0; i < length; i++) {
//                        System.out.println(temp.get(i) + " " + gr);
                        if (!temp.get(i).equals(gr)) {
//                            System.out.println("START: " + startIndex + " END: " + i);
                            groupByOneAttribute(groupByAttributes.get(j), startIndex, i);
                            gr = temp.get(i);
                            startIndex = i;
                        }
                    }
                }
            }
            System.out.println("GROUP BY DONE");
        }

    }

    public int groupByOneAttribute(String groupAttribute, int startIndex, int endIndex){
        String tableName = groupAttribute.split("\\.")[0];
        ArrayList <Integer> tableIDS = finalIDs.get(tableName);

        System.out.println(groupAttribute);
        System.out.println(startIndex);
        int currentGroupNumber = finalIDs.get("groupBy").get(startIndex); //old groupNumber;
        HashMap<Integer, Integer> idsAndGroups = getGroups(groupAttribute);

        //System.out.println("gb: " + currentGroupNumber);
        ArrayList<Integer> usedGroupNumbers = new ArrayList<>();
        usedGroupNumbers.add(currentGroupNumber); //groupNumber after new grouping

//        int firstIndexSort = startIndex;
        int i = startIndex + 1;
        while (i< endIndex){
            int newGroupNumber = idsAndGroups.get(tableIDS.get(i));
            if( !usedGroupNumbers.contains(newGroupNumber)){
                currentGroupNumber++;
                finalIDs.get("groupBy").set(i, currentGroupNumber);
                int nr = updateGroups(i + 1, endIndex, currentGroupNumber,newGroupNumber, tableName, idsAndGroups);
                usedGroupNumbers.add(newGroupNumber);
                i+=nr+1;
            } else {
//                if(i - firstIndexSort > 1 ){
//                    swapRows(firstIndexSort+1, i);
//                    firstIndexSort++;
//                }
                i++;
            }
        }

        return currentGroupNumber;
    }

    //update groupNumbers where oldGroupNumber  == currentGroupNumber and newGroupNumber == newGroupNumber
    public int updateGroups(int currentIndex, int endIndex, int currentGroupNumber, int newGroupNumber, String tableName,HashMap<Integer, Integer> idsAndGroups){

        int rowPlus = 0;
        int temp = currentIndex;
        ArrayList <Integer> tableIDS = finalIDs.get(tableName);
        for(int i = currentIndex; i< endIndex; i++){
            //System.out.print(i + " - " );
            //System.out.println(tableIDS.get(i));
            int grNum = idsAndGroups.get(tableIDS.get(i));
            if(grNum == newGroupNumber){
                finalIDs.get("groupBy").set(i, currentGroupNumber);
                    rowPlus++;
                    swapRows(temp, i);
                    temp++;

            }
        }

        return rowPlus;
    }

    public void swapRows(int insertTo, int insertFrom){
        ArrayList<Integer> rowToMove = new ArrayList<>();
        for ( String key : finalIDs.keySet() ) {
            rowToMove.add(finalIDs.get(key).get(insertFrom));
        }

        for( int i = insertFrom ;i > insertTo ; i--){
            for ( String key : finalIDs.keySet() ) {
                finalIDs.get(key).set(i, finalIDs.get(key).get(i-1));

            }
        }

        int i = 0;
        for ( String key : finalIDs.keySet() ) {
            finalIDs.get(key).set(insertTo, rowToMove.get(i));
            i++;

        }

    }



    //Make a hash with ids and its groupNumber, grouping by groupAttribute
    public HashMap<Integer, Integer> getGroups(String groupAttribute){
        HashMap<Integer, Integer> idsAndGroups = new HashMap<>();//tableID - groupNumber
        String tableName = groupAttribute.split("\\.")[0];
        String attribute = groupAttribute.split("\\.")[1];
        int groupNumber = 1;
        //go through indexFile and get the groups
        if(readXMLFile.isIndex(currentDBName, tableName, attribute)){
            MongoCollection<Document> groupAttributeCollection = mongoDatabase.getCollection(tableName + "." + attribute + ".ind");
            FindIterable<Document> iterable = groupAttributeCollection.find();
            try (MongoCursor<Document> cursor = iterable.iterator()) {
                while (cursor.hasNext()) {
                    Document tempDoc = cursor.next();
                    String[] values = tempDoc.get("value").toString().split("#");
                    for (String value : values) {
                        idsAndGroups.put(Integer.parseInt(value), groupNumber);
                    }
                    groupNumber++;
                }
            }
        } else {//make a list like an indexFile and make the groups
            HashMap<String, String> tempList = new HashMap<>();//groupBy attribute - groupNumber
            MongoCollection<Document> collection = mongoDatabase.getCollection(tableName);
            FindIterable<Document> iterable = collection.find();
            try (MongoCursor<Document> cursor = iterable.iterator()) {
                while (cursor.hasNext()) {
                    Document tempDoc = cursor.next();
                    String collectionValues = tempDoc.get("value").toString();
                    String key = readXMLFile.getAttributeValue(currentDBName, tableName, attribute, collectionValues);
                    String id = tempDoc.get("_id").toString();
                    tempList.merge(key, id, (a, b) -> a + "#" + b);//If key exists updates else assert the given value

                    groupNumber++;
                }
            }

            Iterator it = tempList.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry)it.next();
                String[] ids = pair.getValue().toString().split("#");
                it.remove(); // avoids a ConcurrentModificationException
                for (String id : ids) {
                    idsAndGroups.put(Integer.parseInt(id), groupNumber);
                }
                groupNumber++;
            }
        }

        return idsAndGroups;
    }

//---------------XML FILE HANDLER FUNCTION-------------------------------------------------

    public void trans() {
        try {
            transformer.transform(source, result);
        } catch (TransformerException e) {
            e.printStackTrace();
        }

    }

    public void wClose() {
        pw.close();
    }


}
