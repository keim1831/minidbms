package application;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.util.ArrayList;

import static com.mongodb.client.model.Filters.eq;

public class ReadXMLFile
{
    private org.w3c.dom.Document doc;
    private MongoClient mongoClient;

    public ReadXMLFile(MongoClient mongoClient, org.w3c.dom.Document doc){
        this.mongoClient = mongoClient;
        this.doc = doc;
        try {
            File file = new File("./src/file.xml");
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db  = dbf.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    public Node getDataBase(String dbName){
        NodeList dbNodes = doc.getElementsByTagName("DataBase");
        int x;
        //System.out.println(dbNodes.getLength());

        for( x=0; x<dbNodes.getLength(); x++) {
            String name = dbNodes.item(x).getAttributes().getNamedItem("dataBaseName").getNodeValue();

            if(name.equals(dbName)){
                return dbNodes.item(x);

            }
        }
        System.out.println("DBNAME: " + dbName);
        return null;

    }

    public ArrayList<String> getAllIndexFiles(String dbName){
        NodeList dbNodes = getDataBase(dbName).getChildNodes();

        ArrayList<String> indexes = new ArrayList<>();
        for(int x=0; x < dbNodes.getLength(); x++){
            String table  = dbNodes.item(x).getAttributes().getNamedItem("tableName").getNodeValue();
            Node idx = getTableAttr(dbName, table, "indexFiles");
            if(idx!=null) {
                NodeList idxfl = idx.getChildNodes();
                for (int i = 0; i < idxfl.getLength(); i++) {
                    indexes.add(table + "." + idxfl.item(i).getTextContent() + ".ind");
                }
            }
        }

        return indexes;
    }

    public ArrayList<String> getIndexFiles(String dbName, String tableName){
        if(getTableAttr(dbName,tableName,"indexFiles")!=null) {
            NodeList indx = getTableAttr(dbName, tableName, "indexFiles").getChildNodes();

            ArrayList<String> indexes = new ArrayList<>();
            for (int i = 0; i < indx.getLength(); i++) {
                indexes.add(tableName + "." + indx.item(i).getTextContent() + ".ind");
            }

            return indexes;
        }else {
            return null;
        }
    }


    public Node getTable(String dbName, String tableName){
        //System.out.println("table: " + getDataBase(dbName).getNodeName());
        NodeList table = getDataBase(dbName).getChildNodes();
        boolean ok = false;
        int x=0;
        //System.out.println(table.getLength());
        if(table!=null) {
            for (x = 0; x < table.getLength(); x++) {
                //System.out.println(table.item(x).getAttributes());
                String name = table.item(x).getAttributes().getNamedItem("tableName").getNodeValue();
                //System.out.println(name);
                if (name.equals(tableName)) {
                    ok = true;
                    break;
                }
            }
        }
        if(ok){
            return table.item(x);
        }else {
            return null;
        }
    }

    public Node getTableAttr(String dbName, String tableName, String str){
        if(getTable(dbName,tableName) == null ){
            return null;
        }
        NodeList attr = getTable(dbName,tableName).getChildNodes();
        boolean ok = false;
        int x=0;
        if(attr!=null) {
            for (x = 0; x < attr.getLength(); x++) {
                String name = attr.item(x).getNodeName();
                if (name.equals(str)) {
                    ok = true;
                    break;
                }
            }
        }
        if(ok){
            return attr.item(x);
        }else {
            return null;
        }

    }

    public boolean isFk(String dbName, String tableName, String attribute){
        if(getTableAttr(dbName,tableName,"foreignKey") == null)
            return false;
        NodeList fk = getTableAttr(dbName,tableName,"foreignKey").getChildNodes();
        for (int i=0; i<fk.getLength(); i++){
            String name = fk.item(i).getTextContent();
            if(name.equals(attribute)){
                return true;
            }
        }
        return false;

    }
    public boolean isUnique(String dbName, String tableName, String attribute){
        if(getTableAttr(dbName,tableName,"uniqueKeys") == null)
            return false;
        NodeList fk = getTableAttr(dbName,tableName,"uniqueKeys").getChildNodes();
        for (int i=0; i<fk.getLength(); i++){
            String name = fk.item(i).getTextContent();
            if(name.equals(attribute)){
                return true;
            }
        }
        return false;
    }
    public boolean isPk(String dbName, String tableName, String attribute){
        if(getTableAttr(dbName,tableName,"primaryKey") == null)
            return false;
        NodeList fk = getTableAttr(dbName,tableName,"primaryKey").getChildNodes();
        for (int i=0; i<fk.getLength(); i++){
            String name = fk.item(i).getTextContent();
            if(name.equals(attribute)){
                return true;
            }
        }
        return false;
    }

    public boolean isIndex(String dbName, String tableName, String attribute){
        if(getTableAttr(dbName,tableName,"indexFiles") == null)
            return false;
        NodeList fk = getTableAttr(dbName,tableName,"indexFiles").getChildNodes();
        for (int i=0; i<fk.getLength(); i++){
            String name = fk.item(i).getTextContent();
            if(name.equals(attribute)){
                return true;
            }
        }
        return false;
    }

    public boolean fkConstraintInsert(String dbName, String tableName, String attribute, String key){
        if(getTableAttr(dbName,tableName,"foreignKey")==null){
            return true;
        }
        NodeList fk = getTableAttr(dbName,tableName,"foreignKey").getChildNodes();
        int x;

            for (x = 0; x < fk.getLength(); x++) {
                String name = fk.item(x).getTextContent();
                if (name.equals(attribute)) {
                    MongoDatabase mongoDatabase = mongoClient.getDatabase(dbName);
                    String refT = fk.item(x).getAttributes().getNamedItem("refTable").getNodeValue();
                    String refA = fk.item(x).getAttributes().getNamedItem("refAttribute").getNodeValue();
                    if(isPk(dbName, tableName, refA)) {
                        refA = "_id";
                        MongoCollection<org.bson.Document> collection = mongoDatabase.getCollection(refT);
                        long result = collection.countDocuments(eq(refA, key));
                        if(result == 0) {
                            System.out.println("INSERT: Foreign key constraint failed");
                            return false;
                        }
                        else
                            return true;
                    } else {

                    }

                }
            }
        return true;
    }

    public boolean fkConstraintDelete(String dbName, String tableName, org.bson.Document row){
    NodeList tables = getDataBase(dbName).getChildNodes();
        for(int i =0; i < tables.getLength(); i++) {
            String tbName = tables.item(i).getAttributes().getNamedItem("tableName").getNodeValue();
            if(getTableAttr(dbName, tbName, "foreignKey")!=null) {
                NodeList fk = getTableAttr(dbName, tbName, "foreignKey").getChildNodes();

                for (int x = 0; x < fk.getLength(); x++) {
                    String name = fk.item(x).getAttributes().getNamedItem("refTable").getNodeValue();
                    if (name.equals(tableName)) {
                        MongoDatabase mongoDatabase = mongoClient.getDatabase(dbName);
                        String refA = fk.item(x).getAttributes().getNamedItem("refAttribute").getNodeValue();
                        String delatr = row.getString(refA);
                        MongoCollection<org.bson.Document> collection = mongoDatabase.getCollection(tbName);
                        long result = collection.countDocuments(eq(refA, delatr));
                        if (result == 0) {
                            return true;
                        } else {
                            System.out.println("DELETE: FK constraint failed");
                            return false;
                        }

                    }
                }
            }
        }

        return true;
    }


    public boolean pkConstraint( String dbName, String tableName, String attribute, String key){

        NodeList pk = getTableAttr(dbName,tableName,"primaryKey").getChildNodes();
        int x;
        boolean ok=false;
        if(pk!=null) {
            for (x = 0; x < pk.getLength(); x++) {
                String name = pk.item(x).getTextContent();
                if (name.equals(attribute)) {
                    MongoDatabase mongoDatabase = mongoClient.getDatabase(dbName);
                    String primaryK = pk.item(x).getTextContent();
                    MongoCollection<org.bson.Document> collection = mongoDatabase.getCollection(tableName);
                    long result = collection.countDocuments(eq(primaryK, key));

                    if(result == 0) {
                        return true;
                    }
                    else {
                        System.out.println("Primary key already exits");
                        return false;
                    }

                }
            }
        }
        return true;

    }

    public ArrayList<String> getAllAttributes(String dbName, String tableName){
        NodeList atr = getTableAttr(dbName,tableName,"Structure").getChildNodes();
        int x;
        ArrayList <String> attributes = new ArrayList<>();
        if(atr!=null) {
            for (x = 0; x < atr.getLength(); x++) {
                attributes.add(atr.item(x).getAttributes().getNamedItem("name").getNodeValue());
            }
        }
        return attributes;
    }

    public String getAttributeValue(String dbName, String tableName, String attribute, String value){
        ArrayList<String> attr = getAllAttributes(dbName,tableName);


        String[] values = value.split("#");
        for (int i = 1; i < attr.size() ; i++) {

            if(attr.get(i).equals(attribute)){
                return values[i-1];
            }
        }
        return null;
    }

    public boolean checkType(String dbName, String tableName, String attribute, String type){
        NodeList atr = getTableAttr(dbName,tableName,"Structure").getChildNodes();
        int x;
        if(atr!=null) {
            for (x = 0; x < atr.getLength(); x++) {
                String attrName = atr.item(x).getAttributes().getNamedItem("name").getNodeValue();
                String attrType = atr.item(x).getAttributes().getNamedItem("type").getNodeValue();
                //System.out.println("ATTR: " + attrName + " type: " + attrType+"-----"+ attribute +" : " + type);
                if(attrName.equals(attribute) && attrType.equals(type)){
                    return true;
                }
            }
        }
        return false;
    }


    public static void main(String argv[])
    {
        try
        {
//creating a constructor of file class and parsing an XML file
            File file = new File("./src/file.xml");
//an instance of factory that gives a document builder
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
//an instance of builder to parse the specified xml file
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(file);
            doc.getDocumentElement().normalize();

            System.out.println("Root element: " + doc.getDocumentElement().getNodeName());
            NodeList nodeList = doc.getElementsByTagName("student");
// nodeList is not iterable, so we are using for loop
            for (int itr = 0; itr < nodeList.getLength(); itr++)
            {
                Node node = nodeList.item(itr);
                System.out.println("\nNode Name :" + node.getNodeName());
                if (node.getNodeType() == Node.ELEMENT_NODE)
                {
                    Element eElement = (Element) node;
                    System.out.println("Student id: "+ eElement.getElementsByTagName("id").item(0).getTextContent());
                    System.out.println("First Name: "+ eElement.getElementsByTagName("firstname").item(0).getTextContent());
                    System.out.println("Last Name: "+ eElement.getElementsByTagName("lastname").item(0).getTextContent());
                    System.out.println("Subject: "+ eElement.getElementsByTagName("subject").item(0).getTextContent());
                    System.out.println("Marks: "+ eElement.getElementsByTagName("marks").item(0).getTextContent());
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
