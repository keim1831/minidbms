package application;
import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Router {
	private Stage stage;
	private State state;
	
	public Router(Stage stage,State state) {
		this.stage = stage;
		this.state = state;
	}
	
	public void toWelcome() throws IOException {
		openFxmlAndSetRouter("Welcome.fxml");
	}
	
	public void toDatabase() throws IOException{
		openFxmlAndSetRouter("Welcome.fxml");
		openFxmlAndSetRouter("Databases.fxml");
	}
		

	public void toTable() throws IOException{
		openFxmlAndSetRouter("Table.fxml");
	}
	
	public void toOption() throws IOException{
		openFxmlAndSetRouter("Option.fxml");
	}
	
	public void toInsert() throws IOException{
		openFxmlAndSetRouter("Insert.fxml");
	}
	
	public void toSelect() throws IOException{
		openFxmlAndSetRouter("Select.fxml");
	}
	
	public void toDrop() throws IOException{
		openFxmlAndSetRouter("Drop.fxml");
	}
	
	public void toDropDB() throws IOException{
		openFxmlAndSetRouter("DropDB.fxml");
	}
	
	public void toCreateIndex() throws IOException{
		openFxmlAndSetRouter("CreateIndex.fxml");
	}
	
	public void toFinish() throws IOException{
		openFxmlAndSetRouter("Finish.fxml");
	}
	
	
	public void exit() throws IOException{
		stage.close();
	}
	
	private void openFxmlAndSetRouter(String fileName) throws IOException {
		var url = getClass().getResource(fileName);
		var loader = new FXMLLoader();
		loader.setLocation(url);
		
		Parent view = loader.load(url.openStream());		
		Scene scene = new Scene(view);
		var controller = (IMainViewController) loader.getController();
		
		controller.load(this,state);	
	
		
		stage.setScene(scene);
	}
	
	
}
