package application;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.*;

public class Main extends Application{
	@Override
	public void start(Stage primaryStage) {
		try {
			primaryStage.setTitle("Projekt");
						
			var state = new State();
			var router = new Router(primaryStage,state);
			router.toWelcome();			
			primaryStage.show();

		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	/*
	  public static void main(String argv[]) {
	        WriteXMLFile w = new WriteXMLFile();
	        String str="Company";
	        w.createDB(str);
	        String[] arg = {"114", "geo.kv","geo"};
	        String[] attr = {"poi_id", "0 ","char ","64"};
	        String[] attr1 = {"latitude", "0 ","double ","0"};
	        w.createTable(arg);
	        w.createTableStructure(attr);
	        w.createTableStructure(attr1);
	        String[] fk = {"poi_id ", "geo"," po_id"};
	        String[] pk = {"latitude"};
	        w.createPKFK(pk,fk);

	        w.trans();
	    }
		*/
	public static void main(String[] args) {
		launch(args);
		
	}

}
