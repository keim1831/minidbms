package application;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

public class InsertController implements IMainViewController {
	
	

	private Router router;
	State state;
	
	@FXML Label usedTableLabel;
	@FXML TableView<ArrayList<String>> table;
	
	public void currentT() {
		String str = state.getT().getCurrentTable();
		usedTableLabel.setText(str);
	}

	@Override
	public void load(Router router, State state) {
		this.router = router;
		this.state = state;
		currentT();
	}
	
	@FXML
	private void back(ActionEvent event) throws IOException {
		router.toOption();
	}
	

	@FXML
	private void toTables(ActionEvent event) throws IOException{
		router.toTable();
	}
	
	@FXML
	private void toDatabase(ActionEvent event) throws IOException{
		router.toDatabase();
	}
	
	
	@FXML
	private void next(ActionEvent event) throws IOException{
		router.toFinish();
	}

	@FXML
	private void save(ActionEvent event) throws Exception{
		router.toInsert();
	}
	


	@FXML 
	public void initialize() throws Exception{
		ArrayList<String> ar = new ArrayList<String>();
		Document doc = null;
		//File file = null;
		DocumentBuilderFactory dbf = null;
		DocumentBuilder db = null;
		try   
		{  
		//creating a constructor of file class and parsing an XML file  
			//file = new File("src\\file.xml");  
			//an instance of factory that gives a document builder  
			dbf = DocumentBuilderFactory.newInstance();  
			//an instance of builder to parse the specified xml file  
			db = dbf.newDocumentBuilder();  
			doc = db.parse(new File("src\\file.xml"));  
			doc.getDocumentElement().normalize();  
			//System.out.println("Root element: " + doc.getDocumentElement().getNodeName());  
			NodeList nodeList = doc.getElementsByTagName("Table");  
			
			for (int itr = 0; itr < nodeList.getLength(); itr++)   
			{  
				Element e = (Element)nodeList.item(itr);
		        var description = e.getAttribute("tableName");
		        System.out.println(description);
		        
		        if(description.equals("geo")) {
		        	System.out.println("megvan a geo");
		        	NodeList tableChild = nodeList.item(itr).getChildNodes();
		        	NodeList attribute = tableChild.item(0).getChildNodes();
		        	for (int i = 0; i <attribute.getLength(); i++) {
		        		Element e2 = (Element)attribute.item(i);
				        var d = e2.getAttribute("name");
				        System.out.println(d);
				        ar.add(d);
		        	}
		        	break;
		        }
			}
			doc = null;
			//File file = null;
			dbf = null;
			db = null;
			
		}     
		catch (Exception e)   
		{  
			e.printStackTrace();  
		}  
		
		table.setEditable(true);

	        for (int i = table.getColumns().size(); i < ar.size(); i++) {
	            TableColumn<ArrayList<String>, String> col = new TableColumn<>(ar.get(i));
	            final int colIndex = i ;
	            col.setCellValueFactory(data -> {
	                ArrayList<String> rowValues = data.getValue();
	                String cellValue ;
	                if (colIndex < rowValues.size()) {
	                    cellValue = rowValues.get(colIndex);
	                } else {
	                     cellValue = "" ;
	                }
	                return new ReadOnlyStringWrapper(cellValue);
	            });
	            table.getColumns().add(col);
	        }

	       
		
	}
}
