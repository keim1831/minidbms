package application;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class DatabaseController implements IMainViewController {
    private static final int PORT = 4556;
    BufferedReader input = new BufferedReader(
            new InputStreamReader(System.in));
   
    
    

	private Router router;
	private State state;
	private org.w3c.dom.Document doc;
	
	@FXML private TextField textF;
	@FXML private Button saveButton;
	@FXML private ChoiceBox<String> choiceBox;
	@FXML private Label selected;
	@FXML private Button proba;
	@FXML private CheckBox checkBox;
	@FXML private Button nextButton;
	
	
	public void settingCurrentDB(String str) {
		DataBase db = new DataBase();
		db.setCurrentDB(str);
		state.setDb(db);
	}
	
	public void saveDB(String str) {
		SavedDataBase s = new SavedDataBase();
		s.setSavedDB(str);
		state.setS(s);
	}
	
    private org.w3c.dom.Document currentDoc() {
    	 var doc = state.getDoc(); 
    	 return doc;
    	 
    }
    
    
	public DatabaseController() throws SAXException, IOException {
		 
	    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = null;
        try {
            docBuilder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        File fXmlFile = new File("./src/file.xml");
        doc = docBuilder.parse(fXmlFile);
        doc.getDocumentElement().normalize();
        
	}


	@Override
	public void load(Router router, State state) {
		this.router = router;
		this.state = state;
		doc = currentDoc();
		
	}
	
	@FXML
	private void nextButton(ActionEvent event) throws Exception {
		router.toTable();
	}
	
	

	@FXML
	private void toBack(ActionEvent event) throws IOException{
		router.toWelcome();
	}
	
	@FXML
	private void ckeckIfChecked() {

                 if (selected.getText().length() > 0) {
             		nextButton.setVisible(true);

                	 nextButton.setDisable(false);

		        	//settingCurrendDB(selected.getText());

                	 //s.setDatabase(selected.getText());

                 }

	}

	
	
	@FXML
	private void saveNewDatabase(ActionEvent event) throws IOException, SAXException{
		
		String text = textF.getText();
		
		if(text != null) {
			saveDB(text);
			 try(Socket socket= new Socket("127.0.0.1",PORT)){
		            DataOutputStream output = new DataOutputStream(socket.getOutputStream());
		            
		            //if(state.getS() != null) {
		            	//System.out.println(textF.getText());
		        		output.writeUTF("create database " + textF.getText());
		        		//state.setDoc();
		        		socket.close();
				    	router.toDatabase();
		            //}
		           
		        }catch (UnknownHostException e) {
		            System.out.println("ERROR! Server not found!");
		        }
		        catch (IOException e2) {

		        }
		}
	}
	
	private ArrayList<String> getList() throws Exception{
		
		
		//ArrayList<String> ar = new ArrayList<String>();
		/*doc = null;
		factory = null;
		docBuilder = null;
		f =null;
		try   
		{  
		//creating a constructor of file class and parsing an XML file  
			file = new File("src\\file.xml");  
			//an instance of factory that gives a document builder  
			dbf = DocumentBuilderFactory.newInstance();  
			//an instance of builder to parse the specified xml file  
			db = dbf.newDocumentBuilder(); 
			doc = db.parse(file);  
			doc.getDocumentElement().normalize();  
			//System.out.println("Root element: " + doc.getDocumentElement().getNodeName());  
			NodeList nodeList = doc.getElementsByTagName("DataBase");  
		
			for (int itr = 0; itr < nodeList.getLength(); itr++)   
			{  
				Element e = (Element)nodeList.item(itr);
		        var description = e.getAttribute("dataBaseName");
		        ar.add(description);
			
			}
			doc = null;
			//File file = null;
			dbf = null;
			db = null;
			
		}     
		catch (Exception e)   
		{  
		}  
		*/
		
		NodeList dbNodes = doc.getElementsByTagName("DataBase");
        int x;
        //System.out.println(dbNodes.getLength());
        ArrayList<String> databases = new ArrayList<>();
        for( x=0; x<dbNodes.getLength(); x++) {
            String name = dbNodes.item(x).getAttributes().getNamedItem("dataBaseName").getNodeValue();
            databases.add(name);
/*
            if(name.equals(dbName)){
                return dbNodes.item(x);

            }*/
        }	
        
        //for (String i : databases) {
        	//System.out.println(i); 
        //}
		return databases;
	}
	
	@FXML
	protected void initialize() throws Exception {
		System.out.println("DB Controller");
		nextButton.setVisible(false);
		var choices = FXCollections.observableArrayList(getList());
		choiceBox.setItems(choices);
		choiceBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
		      @Override
		      public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
		        try {
		        	//setDatabase(newValue);
		        	selected.setText(newValue);
		        	settingCurrentDB(newValue);
				} catch (Exception e) {
				}
		      }
		    });
		
		 
	}


	
	/*
	public String getDatabase() {
		return database;
	}
	public void setDatabase(String database) {
		this.database = database;
	}
	*/
	

}

