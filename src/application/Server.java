package application;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server {
    private static final int PORT = 4556;
    private static WriteXMLFile w;
    private static DataInputStream input;

    public static void createDB(String dbName){
        w.createDB(dbName);
    }

    public static void createTable(String tableName) throws IOException {
        w.setCurrentDBName("University");
        System.out.println("TABLE");
        int i;
        String[] msg1 = input.readUTF().split(" ");

        int len = Integer.parseInt(msg1[0]);

        i = 1;
        ArrayList<ArrayList<String>> attrS = new ArrayList<>(100);
        System.out.println(msg1.length);
        while (i<msg1.length) {
            ArrayList<String> attr = new ArrayList<>();
            for (int j = 0; j < 4 ; j ++) {
                attr.add(msg1[i]);
                System.out.println(msg1[i]);
                i++;
            }
            System.out.println();
            attrS.add(attr);
        }
        System.out.println("Out");
        w.createTable(tableName, attrS);
        System.out.println("TABLE CREATED");

    }

    public static void createIndex(String[] msg){
        System.out.println("INDEX");
        String [] indexAtr = new String[msg.length-3];
        int j=0, i;
        //System.out.println("msgL: " + msg.length);
        for (i=3;i<msg.length;i++){
            indexAtr[j]=msg[i];
            j++;
        }
        //System.out.println("length:"+indexAtr.length);
        w.setCurrentDBName("University");
        w.createIndex(indexAtr,msg[2]);
       // w.newIndex(msg[2],msg[3]);

    }

    private static void createFK(String[] msg){
        String[] attr = new String[msg.length-2];
        int j=0, i;
        for(i=3; i<msg.length; i++,j++){
            attr[j] = msg[i];
        }
        //System.out.println(msg);
        w.setCurrentDBName("University");
        w.createFK(msg[2],attr);
    }

    public static void createPK(String[] msg){
        String[] attr = new String[msg.length-2];
        int j=0, i;
        for(i=3; i<msg.length; i++,j++){
            attr[j] = msg[i];
        }
        //System.out.println(msg);
        w.setCurrentDBName("University");
        w.createPK(msg[2],attr);
        //System.out.println("PK added: " + i );
    }

    public static void createUnique(String[] msg){
        String[] attr = new String[msg.length-2];
        int j=0, i;
        for(i=3; i<msg.length; i++,j++){
            attr[j] = msg[i];
        }
        //System.out.println(msg);
        w.uniqueConstraint(attr,msg[2]);
    }

    public static void insert(String[] msg){
        w.setCurrentDBName("University");
        System.out.println("insert data..");
        int i;
        ArrayList<String> insert = new ArrayList<>();

        int len =0;
        for( i = 2; i<msg.length; i++){
            insert.add(msg[i]);
            len++;
        }
        w.insert(msg[1],insert, len);
    }

    public static void select(DataOutputStream out,String[] msg) throws IOException {
        System.out.println("SELECT");
        w.setCurrentDBName("University");

        int i=1;
        ArrayList<String> projectionAttributes = new ArrayList<>();
        System.out.print("SELECT ");
        while (!msg[i].equals("FROM")){
            projectionAttributes.add(msg[i]);
            System.out.print(msg[i] + " ");
            i++;
        }

        System.out.print("\n" + msg[i] + " ");
        i++;

        String tableName = msg[i];
        System.out.println(msg[i]);

        i++;

        ArrayList<ArrayList<String>> joinConditions = new ArrayList<>();

        if(i < msg.length && msg[i].equals("JOIN")) {
            boolean joinDone = !msg[i].equals("JOIN") ;

            while (!joinDone && i < msg.length - 1) {
                System.out.print(msg[i] + " ");
                i++;
                ArrayList<String> condition = new ArrayList<>();
                condition.add(msg[i]);
                System.out.print(msg[i] + "\n" + msg[i+1] + " ");
                condition.add(msg[i+2]);
                System.out.print(msg[i+2] + " = ");
                condition.add(msg[i+4]);
                System.out.println(msg[i+4]);
                i+=5;
                joinConditions.add(condition);
                joinDone = i>= msg.length || !msg[i].equals("JOIN")  ;
//                if( joinDone ){
//                    break;
//                }
            }
        } else  {
            joinConditions = null;
        }


        ArrayList <ArrayList<String>> conditions = new ArrayList<>();

        if(i < msg.length && msg[i].equals("WHERE")) {
            System.out.print(msg[i] + " ");
            i++;
            while (i < msg.length && !msg[i].equals("GROUP")  ) {
                ArrayList<String> condition = new ArrayList<>();
                while (!msg[i].equals("AND") && !msg[i].equals("GROUP")) {
                    condition.add(msg[i]);
                    System.out.print(msg[i] + " ");
                    i++;
                    if (i >= msg.length ) {
                        break;
                    }
                }
                if(i < msg.length && !msg[i].equals("GROUP")) {
                    i++;
                }

                conditions.add(condition);

            }
        } else {
            conditions = null;
        }

        ArrayList<String> groupBy = new ArrayList<>();

        if(i < msg.length && msg[i].equals("GROUP")) {
            System.out.print(msg[i] + " " + msg[i+1] + " ");
            i+=2;

            while (i < msg.length && !msg[i].equals("HAVING") ) {
                groupBy.add(msg[i]);
                System.out.print(msg[i] + " ");
                i ++;
            }
        } else {
            groupBy = null;
        }

        ArrayList<ArrayList<String>> having = new ArrayList<>();

        if(i < msg.length && msg[i].equals("HAVING")) {
            System.out.print("\n" + msg[i] + " ");
            i++;

            while (!msg[i].equals("HAVING") && i < msg.length - 1) {
                ArrayList<String> cond = new ArrayList<>();
                while (!msg[i].equals("AND")) {
                    System.out.print(msg[i] + " ");
                    cond.add(msg[i]);
                    if (i >= msg.length - 1) {
                        break;
                    }
                    i++;
                }
                if(msg[i].equals("AND")) {
                    i++;
                }
                System.out.println();
                having.add(cond);
            }
        } else {
            having = null;
        }

        System.out.println();
        w.select(projectionAttributes, tableName, joinConditions, conditions, groupBy, having);
        out.writeUTF("Done");
    }

    public static void drop(String[] msg){
        w.setCurrentDBName("University");
        if (msg[1].equals("database")) {
            w.dropDatabase(msg[2]);
        } else {
            w.dropTable(msg[2]);
        }
    }

    public static void main (String[] args) throws Exception {

        ServerSocket serverSocket = new ServerSocket(PORT);
        w = new WriteXMLFile();
        w.connectMongoDB();

        while(true) {


            Socket socket = serverSocket.accept();
            try {
                input = new DataInputStream(socket.getInputStream());
                DataOutputStream out = new DataOutputStream(socket.getOutputStream());
                String[] msg = input.readUTF().split(" ");


                System.out.println("-------" + msg[0] + " " + msg[1] + "-------");

                switch (msg[0]) {
                    case "create":
                        switch (msg[1]) {
                            case "database": {
                                createDB(msg[2]);
                                w.setCurrentDBName(msg[2]);
                                break;
                            }
                            case "table": {
                                createTable(msg[2]);
                                break;
                            }
                            case "index": {
                                createIndex(msg);
                                break;
                            }
                            case "foreignKey": {
                                createFK(msg);
                                break;
                            }
                            case "primaryKey": {
                                createPK(msg);
                                break;
                            }
                            case "uniqueAttributes": {
                                createUnique(msg);
                                break;
                            }

                        }
                        break;
                    case "drop":
                        drop(msg);
                        break;
                    case "insert":
                        insert(msg);
                        break;
                    case "delete":
                        w.delete(msg[1], msg[2], msg[3]);
                        break;
                    case "select":
                        select(out,msg);
                        w.wClose();
                        break;
                    default:
                        System.out.println("NOTOK");
                }


                if (msg[0].toLowerCase().equals("exit")) {
                    System.out.println("Server <terminated>");
                    break;
                }
            } catch (IOException e) {
//                System.out.println("Socket exception");
            }
            socket.close();

        }



        serverSocket.close();

    }

}
