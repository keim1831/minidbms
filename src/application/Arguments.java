package application;

import javafx.beans.property.SimpleStringProperty;

public class Arguments {
	   private SimpleStringProperty name;
	   private SimpleStringProperty type;
	   private SimpleStringProperty size;
	   private SimpleStringProperty keyType;
	public Arguments(String name, String type, String size,
			String keyType) {
		this.name = new SimpleStringProperty(name);
		this.type = new SimpleStringProperty(type);
		this.size = new SimpleStringProperty(size);
		this.keyType = new SimpleStringProperty(keyType);
	}
	
	public String getName() {
		return name.get();
	}

	public String getType() {
		return type.get();
	}

	public String getSize() {
		return size.get();
	}

	public String getKeyType() {
		return keyType.get();
	}

	public void setName(SimpleStringProperty name) {
		this.name = name;
	}
	public void setType(SimpleStringProperty type) {
		this.type = type;
	}
	public void setSize(SimpleStringProperty size) {
		this.size = size;
	}
	public void setKeyType(SimpleStringProperty keyType) {
		this.keyType = keyType;
	}
	   
	   
	   
	   
}
