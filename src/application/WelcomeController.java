package application;
import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;

public class WelcomeController implements IMainViewController {
	private Router router;
	
	@Override
	public void load(Router router, State state) {
		this.router = router;
	}
	
	
	@FXML
	private void openDatabase(ActionEvent event) throws IOException{
		router.toDatabase();
	}
	
	@FXML
	private void exitButton(ActionEvent event) throws IOException{
		router.exit();
	}

}
