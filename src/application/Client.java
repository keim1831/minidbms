package application;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;

import javafx.application.Application;
import javafx.stage.Stage;

public class Client extends Application{
    private static final int PORT = 4556;
   
    @Override
	public void start(Stage primaryStage) {
		try {
			primaryStage.setTitle("Projekt");
						
			var state = new State();
			var router = new Router(primaryStage,state);
			router.toWelcome();			
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
    public static void main(String[] args) {
        //State state = new State();

    	launch(args);
    	/*
        var input = new BufferedReader(
                new InputStreamReader(System.in));

        try(Socket socket= new Socket("127.0.0.1",PORT)){

        	

            DataOutputStream output = new DataOutputStream(socket.getOutputStream());
            socket.close();

        }catch (UnknownHostException e) {
            System.out.println("ERROR! Server not found!");
        }
        catch (IOException e2) {

        }
        */
    }

}
/*
output.writeUTF("drop database Premium");

System.out.println("Creating db..");

output.writeUTF("create database Premium");

System.out.println("Creating table..");
output.writeUTF("create table");
output.writeUTF("114 geo.kv geo 4 poi_id 0 char 64 latitude 0 double 0 longitude 0 double 0");
output.writeUTF("create primaryKey poi_id");
//output.writeUTF("create foreignKey poi_id reft poi_id");
output.writeUTF("create uniqueAttributes geo longitude");

//output.writeUTF("insert geo 2 poi_id 1 latitude 1");
output.writeUTF("insert geo 3 poi_id 1 latitude 1 longitude 1 poi_id 2 latitude 2 longitude 2 poi_id 3 latitude 3 longitude 3 poi_id 3 latitude 3 longitude 3");
//output.writeUTF("delete geo poi_id 2");
//output.writeUTF("create index geo latitude longitude");


//output.writeUTF("drop table geo");
//output.writeUTF("drop database Premium");

System.out.println("Creating table..");
output.writeUTF("create table");
output.writeUTF("300 poi.kv poi 4 poi_id 0 char 64 language 0 int 0 poiName 0 char 255");
output.writeUTF("create primaryKey poi_id language");
output.writeUTF("create foreignKey poi_id geo poi_id");
//output.writeUTF("create uniqueAttributes poi poiName");

//output.writeUTF("insert geo 2 poi_id 1 latitude 1");
output.writeUTF("insert poi 3 poi_id 1 language 1 poiName name1 poi_id 3 language 1 poiName name2 poi_id 22 language 3 poiName name3");
//output.writeUTF("delete geo poi_id 2");
output.writeUTF("delete geo poi_id 1");
output.writeUTF("create index poi poiName");






//DataInputStream dataInput = new DataInputStream(socket.getInputStream());
//String answer = dataInput.readUTF();
//System.out.println("Answer: "+answer);
*/   
