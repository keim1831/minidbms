package application;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class CreateIndexController implements IMainViewController{
	
	private static final int PORT = 4556;
    BufferedReader input = new BufferedReader(
            new InputStreamReader(System.in));
    
  
	
	@FXML
	private TextField column;
	@FXML ChoiceBox<String> argumentChoiceBox;
	
	@FXML  Label used;
	@FXML Label selected;
	
	public Router router;
	State state;
	
	public void currentTable() {
		String str = state.getT().getCurrentTable();
		used.setText(" "+str);
	}

	private ArrayList<String> getList() throws IOException{

		ArrayList<String> ar = new ArrayList<String>();
		Document doc = null;
		//File file = null;
		DocumentBuilderFactory dbf = null;
		DocumentBuilder db = null;
		try   
		{  
		//creating a constructor of file class and parsing an XML file  
			//file = new File("src\\file.xml");  
			//an instance of factory that gives a document builder  
			dbf = DocumentBuilderFactory.newInstance();  
			//an instance of builder to parse the specified xml file  
			db = dbf.newDocumentBuilder();  
			doc = db.parse(new File("src\\file.xml"));  
			doc.getDocumentElement().normalize();  
			//System.out.println("Root element: " + doc.getDocumentElement().getNodeName());  
			NodeList nodeList = doc.getElementsByTagName("Table");  
			
			for (int itr = 0; itr < nodeList.getLength(); itr++)   
			{  
				Element e = (Element)nodeList.item(itr);
		        var description = e.getAttribute("tableName");
		        System.out.println(description);
		        
		        if(description.equals("geo")) {
		        	System.out.println("megvan a geo");
		        	NodeList tableChild = nodeList.item(itr).getChildNodes();
		        	NodeList attribute = tableChild.item(0).getChildNodes();
		        	for (int i = 0; i <attribute.getLength(); i++) {
		        		Element e2 = (Element)attribute.item(i);
				        var d = e2.getAttribute("name");
				        System.out.println(d);
				        ar.add(d);
		        	}
		        	break;
		        }
			}
			doc = null;
			//File file = null;
			dbf = null;
			db = null;
			
		}     
		catch (Exception e)   
		{  
			e.printStackTrace();  
		} 
		
		return ar;
	}
	
	
	@Override
	public void load( Router router, State state) {
		this.router = router;
		this.state = state;
		currentTable();
	}
	
	
	@FXML
	protected void initialize() throws Exception {
		var choices = FXCollections.observableArrayList(getList());
		argumentChoiceBox.setItems(choices);
		argumentChoiceBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
		      @Override
		      public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
		        try {
		        	selected.setText(newValue);
		        	
		        			        
				} catch (Exception e) {
				}
		      }
		    });
	}
	
	@FXML
	private void back(ActionEvent event) throws IOException {
		router.toOption();
	}
	

	@FXML
	private void toTables(ActionEvent event) throws IOException{
		router.toTable();
	}
	
	@FXML
	private void toDatabase(ActionEvent event) throws IOException{
		router.toDatabase();
	}
	
	
	@FXML
	private void next(ActionEvent event) throws IOException{
		router.toFinish();
	
	}
	@FXML 
	private void saveButton(ActionEvent event) throws Exception{
		try(Socket socket= new Socket("127.0.0.1",PORT)){
	        DataOutputStream output = new DataOutputStream(socket.getOutputStream());
	        
	        output.writeUTF("create index"+used.getText()+" "+  selected.getText());
	        
	        socket.close();
	    }catch (UnknownHostException e) {
	        System.out.println("ERROR! Server not found!");
	    }
	    catch (IOException e2) {

	    }
		
		router.toCreateIndex();
	}


}
